import torch
import os
import time
from codecarbon import EmissionsTracker
from typing import List, Tuple, Dict, Any, Union, Optional, Callable
import csv
import sklearn.metrics
import green_ai
import ptflops
from ptflops import get_model_complexity_info
import numpy as np
import joblib


# train with dataloader, get all logs in a csv file, first column is the id of the log specified by the user
def train_torch(
    model: torch.nn.Module,
    train_loader: torch.utils.data.DataLoader,
    epochs: int,
    optimizer: torch.optim.Optimizer,
    loss_fn: torch.nn.Module,
    log: bool = False,
    log_file: str = "",
    log_id: Optional[Union[int, str]] = 0,
    verbose: int = 0,
    validation_loader: Optional[torch.utils.data.DataLoader] = None,
    patience: int = 5,
) -> Tuple[List[float], List[float], List[float]]:
    """
    Train a PyTorch model using a DataLoader and log the training process to a CSV file.

    Args:
        model (torch.nn.Module): The PyTorch model to train.
        train_loader (torch.utils.data.DataLoader): The DataLoader containing the training data.
        epochs (int): The number of epochs to train for.
        optimizer (torch.optim.Optimizer): The optimizer to use during training.
        loss_fn (torch.nn.Module): The loss function to use during training.
        log (bool): Whether to log the training data to a CSV file.
        log_file (str): The path to the CSV file to log training data to.
        log_id (int): The ID to associate with the training data in the log file.
        verbose (int, optional): The verbosity level. 0 - no output, 1 - print every 3 epochs, 2 - print epoch progress and loss. Defaults to 0.
        validation_loader (Optional[torch.utils.data.DataLoader]): The DataLoader containing the validation data.

    Returns:
        Tuple[List[float], List[float], List[float]]: The losses, times, and carbon emissions for each epoch.
    """
    best_val_loss = np.inf
    model.train()
    verbose_freq = len(train_loader) // 10
    losses = []
    times = []
    carbon = []
    running_loss = 0
    patience_counter = 0
    # Create a CSV file to log the training data
    if log:
        if log_file == "":
            raise ValueError("A log file must be specified if logging is enabled")
        if log_id == 0:
            print("Warning: No log ID specified, using 0 as the default")
        with open(log_file, "a", newline="") as file:
            writer = csv.writer(file)
            writer.writerow(["id", "epoch", "loss", "time", "carbon"])
    # Train the model for the specified number of epochs, getting the loss and time taken for each epoch
    for epoch in range(epochs):
        start_time = time.time()
        tracker = EmissionsTracker(log_level="critical", save_to_file=False)
        tracker.start()
        for i, (inputs, data) in enumerate(train_loader):
            optimizer.zero_grad()
            y_pred = model(inputs)
            loss = loss_fn(y_pred, data)
            loss.backward()
            optimizer.step()
            running_loss += loss.item()
            if verbose == 2 and i % verbose_freq == verbose_freq - 1:
                print(f"[{epoch + 1}, {i + 1}] loss: {running_loss / verbose_freq}")
                running_loss = 0
        end_time = time.time()
        epoch_time = end_time - start_time
        epoch_carbon = tracker.stop()
        # Log the training data for the epoch
        losses.append(loss.item())
        times.append(epoch_time)
        carbon.append(epoch_carbon)
        if log:
            with open(log_file, "a", newline="") as file:
                writer = csv.writer(file)
                writer.writerow([log_id, epoch, loss.item(), epoch_time, epoch_carbon])
        # Perform validation if validation_loader is provided
        if validation_loader is not None:
            model.eval()
            val_loss = 0
            num_val_samples = 0
            with torch.no_grad():
                for val_inputs, val_data in validation_loader:
                    val_outputs = model(val_inputs)
                    val_loss += loss_fn(val_outputs, val_data).item()
                    num_val_samples += val_inputs.size(0)
            avg_val_loss = val_loss / num_val_samples

            if avg_val_loss < best_val_loss:
                print("New best val loss:", avg_val_loss)
                best_val_loss = avg_val_loss
                torch.save(model.state_dict(), "temp.pth")
                patience_counter = 0
            else:
                patience_counter += 1
                if patience_counter == patience:
                    print("Early stopping")
                    break

        if verbose == 1:
            if (epoch + 1) % 3 == 0:
                print(
                    f"Epoch {epoch+1}/{epochs}, Loss: {avg_val_loss if validation_loader is not None else loss.item()}"
                )
        elif verbose == 2:
            print(
                f"Epoch {epoch+1}/{epochs}, Loss: {avg_val_loss if validation_loader is not None else loss.item()}"
            )

        model.train()

    if validation_loader is not None:
        model.load_state_dict(torch.load("temp.pth"))
        os.remove("temp.pth")

    return losses, times, carbon * 1000


def test_torch(
    model: torch.nn.Module,
    test_loader: torch.utils.data.DataLoader,
    loss_fn: torch.nn.Module,
    metric_fn=[Union[Callable, torch.nn.Module]],
    log: bool = False,
    log_file: str = "",
    log_id: int = 0,
) -> tuple[float, float]:
    """
    Test a PyTorch model using a DataLoader and log the testing process to a CSV file.

    Args:
        model (torch.nn.Module): The PyTorch model to test.
        test_loader (torch.utils.data.DataLoader): The DataLoader containing the test data.
        loss_fn (torch.nn.Module): The loss function to use during testing.
        metric_fn (torch.nn.Module): The metric function to use during testing.
        log_file (str): The path to the CSV file to log testing data to.
        log_id (int): The ID to associate with the testing data in the log file.
    Returns:
        float: The average metric value, float: The average loss value
    """
    total_loss = 0
    total_metric = 0
    num_samples = 0

    if log:
        if log_file == "":
            raise ValueError("A log file must be specified if logging is enabled")
        if log_id == 0:
            print("Warning: No log ID specified, using 0 as the default")
        with open(log_file, "a", newline="") as file:
            writer = csv.writer(file)
            writer.writerow(["id", "loss", "metric"])

            # Test the model on the whole test set
    model.eval()
    with torch.no_grad():
        for i, (x, y) in enumerate(test_loader):

            y_pred = model(x)
            loss = loss_fn(y_pred, y).item()

            if metric_fn is sklearn.metrics.accuracy_score:
                metric = metric_fn(y, y_pred.argmax(dim=1))
            else:
                try:
                    metric = metric_fn(y, y_pred)
                except:
                    metric = metric_fn(y, y_pred).item()

            total_loss += loss * x.size(0)
            total_metric += metric * x.size(0)

            num_samples += x.size(0)

    avg_loss = total_loss / num_samples
    avg_metric = total_metric / num_samples
    # Log the testing data
    if log:
        with open(log_file, "a", newline="") as file:
            writer = csv.writer(file)
            writer.writerow([log_id, avg_loss, avg_metric])

    return avg_loss, avg_metric


def time_torch(
    model: torch.nn.Module,
    testloader: torch.utils.data.DataLoader,
    device: str = "cuda" if torch.cuda.is_available() else "cpu",
    num_iterations: int = 10,
    warm_up: int = 5,
) -> tuple[float, float]:
    """
    Time a PyTorch model on a given device.

    Args:
        model (torch.nn.Module): The PyTorch model to time.
        input_shape (Tuple[int]): The shape of the input tensor.
        device (str, optional): The device to run the model on. Defaults to 'cpu'.
        num_iterations (int, optional): The number of iterations to run the model for. Defaults to 10.
        warm_up (int, optional): The number of warm-up iterations to run before timing. Defaults to 5.

    Returns:
        float: The average time taken to run the model for one iteration, float : The average emissions for one iteration
    """
    # Move the model to the specified device
    model.to(device)
    model.eval()

    for i in range(warm_up):
        for inputs, _ in testloader:
            inputs = inputs.to(device)
            _ = model(inputs)

    tracker = EmissionsTracker(log_level="critical", save_to_file=False)
    tracker.start()
    start = time.time()
    for i in range(num_iterations):
        for inputs, _ in testloader:
            inputs = inputs.to(device)
            _ = model(inputs)
    total_carbon = tracker.stop()
    end = time.time()
    total_time = end - start
    avg_time = total_time / num_iterations
    avg_time_per_sample = avg_time / len(testloader.dataset)
    avg_carbon = total_carbon / num_iterations

    return avg_time_per_sample * 1000, avg_carbon * 1000


def calibrate_model(
    model: torch.nn.Module, train_loader: torch.utils.data.DataLoader
) -> None:
    """
    Calibrates the model to find the quantization parameters.

    Args:
        model (Module): The model to be calibrated.
        train_loader (DataLoader): The data loader for the training data.

    Returns:
        None
    """
    model.eval()  # Set the model to evaluation mode
    for inputs, _ in train_loader:  # Run the model on the training data
        _ = model(inputs)  # Forward pass


def get_torch_model_size(model: torch.nn.Module) -> int:
    """
    Get the size of a PyTorch model in bytes.

    Args:
        model (torch.nn.Module): The PyTorch model to get the size of.

    Returns:
        int: The size of the model in MB.
    """
    torch.save(model.state_dict(), "tmp.pt")
    size = os.path.getsize("tmp.pt") / 1e6
    os.remove("tmp.pt")

    return size


def train_torch_kd(
    teacher: torch.nn.Module,
    student: torch.nn.Module,
    train_loader: torch.utils.data.DataLoader,
    epochs: int,
    optimizer: torch.optim.Optimizer,
    loss_fn: torch.nn.Module,
    log: bool = False,
    log_file: str = "",
    log_id: Optional[Union[int, str]] = 0,
    verbose: int = 0,
    temperature=2,
    teacher_weight: float = 0.5,
    validation_loader: Optional[torch.utils.data.DataLoader] = None,
    patience: int = 5,
    classification: bool = False,
) -> Tuple[List[float], List[float], List[float]]:
    """
    Train a PyTorch model using a DataLoader and log the training process to a CSV file.

    Args:
        teacher (torch.nn.Module): The teacher PyTorch model.
        student (torch.nn.Module): The student PyTorch model to train.
        train_loader (torch.utils.data.DataLoader): The DataLoader containing the training data.
        epochs (int): The number of epochs to train for.
        optimizer (torch.optim.Optimizer): The optimizer to use during training.
        loss_fn (torch.nn.Module): The loss function to use during training.
        log (bool, optional): Whether to log the training data to a CSV file. Defaults to False.
        log_file (str, optional): The path to the CSV file to log training data to. Defaults to "".
        log_id (Optional[Union[int, str]], optional): The ID to associate with the training data in the log file. Defaults to 0.
        verbose (int, optional): The verbosity level. 0 - no output, 1 - print every 3 epochs, 2 - print epoch progress and loss. Defaults to 0.
        teacher_weight (float, optional): The weight of the teacher model in the loss function. Defaults to 0.5.

    Returns:
        Tuple[List[float], List[float], List[float]]: The losses, times, and carbon emissions for each epoch.
    """
    best_val_loss = np.inf

    losses = []
    times = []
    carbon = []
    running_loss = 0
    patience_counter = 0
    verbose_freq = len(train_loader) // 10

    student.train()
    teacher.eval()

    # Create a CSV file to log the training data
    if log:
        if log_file == "":
            raise ValueError("A log file must be specified if logging is enabled")
        if log_id == 0:
            print("Warning: No log ID specified, using 0 as the default")
        with open(log_file, "a", newline="") as file:
            writer = csv.writer(file)
            writer.writerow(["id", "epoch", "loss", "time", "carbon"])

    # Train the model for the specified number of epochs, getting the loss and time taken for each epoch
    for epoch in range(epochs):
        start_time = time.time()
        tracker = EmissionsTracker(log_level="critical", save_to_file=False)
        tracker.start()

        for i, (x, y) in enumerate(train_loader):
            optimizer.zero_grad()
            y_pred_student = student(x)

            with torch.no_grad():
                y_pred_teacher = teacher(x)

            if classification:
                soft_targets = torch.nn.functional.softmax(
                    y_pred_teacher / temperature, dim=1
                )
                soft_prob = torch.nn.functional.log_softmax(
                    y_pred_student / temperature, dim=1
                )

                teacher_loss = (
                    torch.sum(soft_targets * (soft_targets.log() - soft_prob))
                    / soft_prob.size()[0]
                    * temperature**2
                )

            else:
                teacher_loss = loss_fn(y_pred_student, y_pred_teacher)

            student_loss = loss_fn(y_pred_student, y)

            loss = teacher_weight * teacher_loss + (1 - teacher_weight) * student_loss

            loss.backward()

            optimizer.step()
            running_loss += loss.item()
            if verbose == 2 and i % verbose_freq == verbose_freq - 1:
                print(f"[{epoch + 1}, {i + 1}] loss: {running_loss / verbose_freq}")
                running_loss = 0

        end_time = time.time()
        epoch_time = end_time - start_time
        epoch_carbon = tracker.stop()

        # Log the training data for the epoch
        losses.append(loss.item())
        times.append(epoch_time)
        carbon.append(epoch_carbon)
        if log:
            with open(log_file, "a", newline="") as file:
                writer = csv.writer(file)
                writer.writerow([log_id, epoch, loss.item(), epoch_time, epoch_carbon])

        if validation_loader is not None:
            student.eval()
            val_loss = 0
            num_val_samples = 0
            with torch.no_grad():
                for val_inputs, val_data in validation_loader:
                    val_outputs = student(val_inputs)
                    val_loss += loss_fn(val_outputs, val_data).item()
                    num_val_samples += val_inputs.size(0)

            avg_val_loss = val_loss / num_val_samples

            if avg_val_loss < best_val_loss:
                print("New best val loss:", avg_val_loss)
                best_val_loss = avg_val_loss
                torch.save(student.state_dict(), "temp.pth")
                patience_counter = 0
            else:
                patience_counter += 1
                if patience_counter == patience:
                    print("Early stopping")
                    break

        if verbose == 1:
            if (epoch + 1) % 3 == 0:
                print(
                    f"Epoch {epoch+1}/{epochs}, Loss: {avg_val_loss if validation_loader is not None else loss.item()}"
                )
        elif verbose == 2:
            print(
                f"Epoch {epoch+1}/{epochs}, Loss: {avg_val_loss if validation_loader is not None else loss.item()}"
            )

        student.train()

    if validation_loader is not None:
        student.load_state_dict(torch.load("temp.pth"))
        os.remove("temp.pth")

    return losses, times, carbon * 1000


def calculate_convlayer_out_size(
    height: int,
    width: int,
    kernel_size: int,
    padding: int,
    stride: int,
    dilation: int,
    pooling_kernel_size: int = 1,
    pooling_stride: int = 1,
) -> Tuple[int, int]:
    """
    Calculate the output size of a convolutional layer.

    Args:
        height (int): The height of the input tensor.
        width (int): The width of the input tensor.
        kernel_size (int): The size of the kernel.
        padding (int): The padding size.
        stride (int): The stride size.
        dilation (int): The dilation size.
        pooling_kernel_size (int, optional): The size of the pooling kernel. Defaults to 1.

    Returns:
        Tuple[int, int]: The height and width of the output tensor.
    """
    out_height = (
        (height + 2 * padding - dilation * (kernel_size - 1) - 1) // stride
    ) + 1
    out_width = ((width + 2 * padding - dilation * (kernel_size - 1) - 1) // stride) + 1

    out_height = ((out_height - pooling_kernel_size) // pooling_stride) + 1
    out_width = ((out_width - pooling_kernel_size) // pooling_stride) + 1

    return out_height, out_width


def get_model_macs(model, input_shape):
    macs, params = get_model_complexity_info(
        model, tuple(input_shape), as_strings=False, print_per_layer_stat=False
    )
    return macs


def get_model_size(model):
    joblib.dump(model, "temp.pkl")
    size = os.path.getsize("temp.pkl") / 1e6  # 1e6 to convert to MB
    os.remove("temp.pkl")
    return size
