import torch
from typing import List, Tuple, Dict, Any, Union, Optional
import copy

import torch.nn.functional
from torch.nn.modules import Module
from torch.optim.optimizer import Optimizer as Optimizer
from green_ai.utils import calculate_convlayer_out_size
import green_ai.utils


class base(torch.nn.Module):
    def __init__(self):
        super(base, self).__init__()
        self.losses = []
        self.times = []
        self.carbons = []
        self.trained = False

    def forward(self, x):
        pass

    def fit_train(
        self: torch.nn.Module,
        train_loader: torch.utils.data.DataLoader,
        epochs: int,
        optimizer: torch.optim.Optimizer,
        loss_fn: torch.nn.Module,
        log: bool = False,
        log_file: str = "",
        log_id: Optional[Union[int, str]] = 0,
        verbose: int = 0,
        validation_loader: Optional[torch.utils.data.DataLoader] = None,
        patience: int = 5,
    ) -> Tuple[List[float], List[float], List[float]]:

        if self.trained:
            print("Model has already been trained.")
            return self.losses, self.times, self.carbons

        self.losses, self.times, self.carbons = green_ai.utils.train_torch(
            model=self,
            train_loader=train_loader,
            validation_loader=validation_loader,
            patience=patience,
            epochs=epochs,
            optimizer=optimizer,
            loss_fn=loss_fn,
            log=log,
            log_file=log_file,
            log_id=log_id,
            verbose=verbose,
        )
        self.trained = True
        return self.losses, self.times, self.carbons

    def reset(self):
        self.losses = []
        self.times = []
        self.carbons = []
        self.trained = False
        # reset weights
        for layer in self.children():
            if hasattr(layer, "reset_parameters"):
                layer.reset_parameters()


class torch_small_NN(base):
    """
    A small neural network module.

    Args:
        input_size (int): The size of the input tensor.
        hidden_size (int): The size of the hidden layer.
        output_size (int): The size of the output tensor.
        hidden_activation (torch.nn.Module, optional): The activation function to use in the hidden layer. Defaults to torch.nn.ReLU().
        output_activation (torch.nn.Module, optional): The activation function to use in the output layer. Defaults to torch.nn.Identity().

    Attributes:
        hidden (torch.nn.Linear): The linear layer for the hidden layer.
        hidden_activation (torch.nn.Module): The activation function for the hidden layer.
        output (torch.nn.Linear): The linear layer for the output layer.
        output_activation (torch.nn.Module): The activation function for the output layer.
    """

    def __init__(
        self,
        input_size: int,
        hidden_size: int,
        output_size: int,
        hidden_activation: torch.nn.Module = torch.nn.ReLU(),
        output_activation: torch.nn.Module = torch.nn.Identity(),
    ):
        super(torch_small_NN, self).__init__()
        self.hidden = torch.nn.Linear(input_size, hidden_size)
        self.hidden_activation = hidden_activation
        self.output = torch.nn.Linear(hidden_size, output_size)
        self.output_activation = output_activation

    def forward(self, x):
        x = self.hidden(x)
        x = self.hidden_activation(x)
        x = self.output(x)
        x = self.output_activation(x)
        return x


class torch_NN(base):
    """
    A custom neural network model implemented using PyTorch.

    Args:
        input_size (int): The size of the input layer.
        hidden_sizes (List[int]): The sizes of the hidden layers.
        output_size (int, optional): The size of the output layer. Defaults to 1.
        hidden_activations (List[torch.nn.Module], optional): The activation functions for the hidden layers. Defaults to [torch.nn.ReLU()].
        output_activation (torch.nn.Module, optional): The activation function for the output layer. Defaults to torch.nn.Identity().

    Attributes:
        input_size (int): The size of the input layer.
        output_size (int): The size of the output layer.
        output_activation (torch.nn.Module): The activation function for the output layer.
        hidden_sizes (List[int]): The sizes of the hidden layers.
        hidden_activations (List[torch.nn.Module]): The activation functions for the hidden layers.
        layers (List[torch.nn.Module]): The layers of the neural network model.
        model (torch.nn.Sequential): The sequential model representing the neural network.

    Methods:
        _verify_hidden_layers(hidden_sizes: List[int], hidden_activations: List[torch.nn.Module]) -> Tuple[List[int], List[torch.nn.Module]]:
            Verify the hidden layers.

        forward(x) -> torch.Tensor:
            Forward pass of the neural network.

        _check_input(hidden_sizes: List[int], hidden_activations: List[torch.nn.Module]):
            Check if the input sizes are correct.
    """

    def __init__(
        self,
        input_size: int,
        hidden_sizes: List[int],
        output_size: int = 1,
        hidden_activations: List[torch.nn.Module] = [torch.nn.ReLU()],
        output_activation: torch.nn.Module = torch.nn.Identity(),
    ):
        """
        Initialize the torch_NN model.

        Args:
            input_size (int): The size of the input layer.
            hidden_sizes (List[int]): The sizes of the hidden layers.
            output_size (int, optional): The size of the output layer. Defaults to 1.
            hidden_activations (List[torch.nn.Module], optional): The activation functions for the hidden layers. Defaults to [torch.nn.ReLU()].
            output_activation (torch.nn.Module, optional): The activation function for the output layer. Defaults to torch.nn.Identity().
        """
        super(torch_NN, self).__init__()
        self._check_input(hidden_sizes, hidden_activations)
        self.input_size = input_size
        self.output_size = output_size
        self.output_activation = output_activation
        hidden_sizes, hidden_activations = self._verify_hidden_layers(
            hidden_sizes, hidden_activations
        )
        self.layers = []
        self.layers.append(torch.nn.Linear(input_size, hidden_sizes[0]))
        self.layers.append(hidden_activations[0])
        for i in range(1, len(hidden_sizes)):
            self.layers.append(torch.nn.Linear(hidden_sizes[i - 1], hidden_sizes[i]))
            self.layers.append(hidden_activations[i])
        self.layers.append(torch.nn.Linear(hidden_sizes[-1], output_size))
        self.layers.append(output_activation)

        self.model = torch.nn.Sequential(*self.layers)

    def _verify_hidden_layers(
        self, hidden_sizes: List[int], hidden_activations: List[torch.nn.Module]
    ) -> Tuple[List[int], List[torch.nn.Module]]:
        """
        Verify the hidden layers.

        Args:
            hidden_sizes (List[int]): The sizes of the hidden layers.
            hidden_activations (List[torch.nn.Module]): The activation functions for the hidden layers.

        Returns:
            Tuple[List[int], List[torch.nn.Module]]: The verified hidden sizes and hidden activations.
        """
        if len(hidden_activations) == 1:
            hidden_activations = hidden_activations * len(hidden_sizes)
        elif len(hidden_activations) != len(hidden_sizes):
            raise ValueError(
                "The number of hidden activations must be either 1 or equal to the number of hidden sizes."
            )
        return hidden_sizes, hidden_activations

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """
        Forward pass of the neural network.

        Args:
            x (torch.Tensor): The input data.

        Returns:
            torch.Tensor: The output of the neural network.
        """
        return self.model(x)

    def _check_input(
        self, hidden_sizes: List[int], hidden_activations: List[torch.nn.Module]
    ):
        """
        Check if the input sizes are correct.

        Args:
            hidden_sizes (List[int]): The sizes of the hidden layers.
            hidden_activations (List[torch.nn.Module]): The activation functions for the hidden layers.
        """
        if len(hidden_activations) == 1:
            hidden_activations = hidden_activations * len(hidden_sizes)
        elif len(hidden_activations) != len(hidden_sizes):
            raise ValueError(
                "The number of hidden activations must be either 1 or equal to the number of hidden sizes."
            )
        self.hidden_sizes = hidden_sizes
        self.hidden_activations = hidden_activations


class torch_QATModel(base):
    """
    A class representing a quantization-aware training model in PyTorch.

    Args:
        base_model (torch.nn.Module): The base model to be quantized.
        qconfig (str, optional): The quantization configuration. Defaults to "fbgemm".

    Attributes:
        base_model (torch.nn.Module): The base model to be quantized.
        qconfig (torch.quantization.QConfig): The quantization configuration.
        quant (torch.quantization.QuantStub): The quantization stub.
        dequant (torch.quantization.DeQuantStub): The dequantization stub.
    """

    def __init__(self, base_model: torch.nn.Module, qconfig: str = "fbgemm"):
        super(torch_QATModel, self).__init__()
        print("Warning: no qconfig provided, using default qconfig")
        self.model = copy.deepcopy(base_model)
        self.qconfig = torch.quantization.get_default_qconfig(qconfig)
        self.quant = torch.quantization.QuantStub()
        self.dequant = torch.quantization.DeQuantStub()

    def forward(self, x):
        """
        Forward pass of the quantization-aware training model.

        Args:
            x: The input tensor.

        Returns:
            The output tensor after passing through the model.
        """
        x = self.quant(x)
        x = self.model(x)
        x = self.dequant(x)
        return x

    def fit_train(
        self: Module,
        train_loader: torch.utils.data.DataLoader,
        epochs: int,
        optimizer: Optimizer,
        loss_fn: Module,
        log: bool = False,
        log_file: str = "",
        log_id: int | str | None = 0,
        verbose: int = 0,
        validation_loader: Optional[torch.utils.data.DataLoader] = None,
        patience: int = 5,
    ) -> Tuple[List[float]]:

        if self.trained:
            print("Model has already been trained.")
            return self.losses, self.times, self.carbons

        torch.quantization.prepare_qat(self, inplace=True)

        self.losses, self.times, self.carbons = green_ai.utils.train_torch(
            model=self,
            train_loader=train_loader,
            epochs=epochs,
            optimizer=optimizer,
            loss_fn=loss_fn,
            log=log,
            log_file=log_file,
            log_id=log_id,
            verbose=verbose,
            validation_loader=validation_loader,
            patience=patience,
        )
        self.eval()
        torch.quantization.convert(self, inplace=True)
        self.trained = True
        return self.losses, self.times, self.carbons

    def reset(self):
        print("Cannot reset a quantized model.")


class SeparableConv2d(torch.nn.Module):
    """
    A class representing a separable convolutional layer in a neural network.

    Args:
        in_channels (int): Number of input cshannels.
        out_channels (int): Number of output channels.
        kernel_size (int or tuple): Size of the convolutional kernel.
        stride (int or tuple, optional): Stride of the convolution. Default is 1.
        padding (int or tuple, optional): Padding added to the input. Default is 0.
        dilation (int or tuple, optional): Spacing between kernel elements. Default is 1.
        bias (bool, optional): Whether to include a bias term. Default is True.
    """

    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        stride=1,
        padding=0,
        dilation=1,
        bias=True,
        depthwise_num: int = 1,  # Number of depthwise convolutions used when applying separable convolutions
    ):
        super(SeparableConv2d, self).__init__()
        self.depthwise = torch.nn.Conv2d(
            in_channels=in_channels,
            out_channels=in_channels * depthwise_num,
            kernel_size=kernel_size,
            stride=stride,
            padding=padding,
            dilation=dilation,
            groups=in_channels,
            bias=bias,
        )
        self.pointwise = torch.nn.Conv2d(
            in_channels=in_channels * depthwise_num,
            out_channels=out_channels,
            kernel_size=1,
            stride=1,
            padding=0,
            dilation=1,
            groups=1,
            bias=bias,
        )

    def forward(self, x):
        x = self.depthwise(x)
        x = self.pointwise(x)
        return x


class small_ConvModel(base):
    def __init__(
        self,
        height: int,
        width: int,
        in_channels: int,
        out_channels: int,
        out_features: int,
        separable: bool = False,
        kernel_size: int = 3,
        stride: int = 1,
        padding: int = 0,
        dilation: int = 1,
        bias: bool = True,
        depthwise_num: int = 1,  # Number of depthwise convolutions used when applying separable convolutions
        output_func: torch.nn.Module = torch.nn.Identity(),
    ):
        super(small_ConvModel, self).__init__()
        if separable:
            self.conv1 = SeparableConv2d(
                in_channels=in_channels,
                out_channels=out_channels // 2,
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
                dilation=dilation,
                bias=bias,
                depthwise_num=depthwise_num,
            )
            self.conv2 = SeparableConv2d(
                in_channels=out_channels // 2,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
                dilation=dilation,
                bias=bias,
                depthwise_num=depthwise_num,
            )
        else:
            self.conv1 = torch.nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels // 2,
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
                dilation=dilation,
                bias=bias,
            )
            self.conv2 = torch.nn.Conv2d(
                in_channels=out_channels // 2,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
                dilation=dilation,
                bias=bias,
            )
        self.separable = separable
        self.pool = torch.nn.MaxPool2d(kernel_size=2, stride=2)
        self.batchnorm_1 = torch.nn.BatchNorm2d(out_channels // 2)
        self.batchnorm_2 = torch.nn.BatchNorm2d(out_channels)
        # Calculate the output size after the convolutional layers
        out_height, out_width = height, width
        for _ in range(2):
            out_height, out_width = calculate_convlayer_out_size(
                height=out_height,
                width=out_width,
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
                dilation=dilation,
                pooling_kernel_size=2,
                pooling_stride=2,
            )

        self.fc = torch.nn.Linear(out_height * out_width * out_channels, out_features)

        self.relu = torch.nn.ReLU(inplace=True)

        self.output_func = output_func

    def forward(self, x):
        x = self.conv1(x)
        x = self.batchnorm_1(x)
        x = self.relu(x)
        x = self.pool(x)
        x = self.conv2(x)
        x = self.batchnorm_2(x)
        x = self.relu(x)
        x = self.pool(x)
        x = torch.flatten(x, 1)
        x = self.fc(x)
        return self.output_func(x)

    def fuse_model_layers(self) -> any:
        if not self.separable:
            modules_to_fuse = [["conv1", "batchnorm_1"], ["conv2", "batchnorm_2"]]
        else:
            modules_to_fuse = [
                ["conv1.pointwise", "batchnorm_1"],
                ["conv2.pointwise", "batchnorm_2"],
            ]
        for modules in modules_to_fuse:
            torch.quantization.fuse_modules(self, modules, inplace=True)


class ConvLayer(torch.nn.Module):
    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        kernel_size: int = 3,
        separable: bool = True,
    ):
        super(ConvLayer, self).__init__()
        self.separable = separable
        if separable:
            self.conv1 = SeparableConv2d(
                in_channels=in_channels,
                out_channels=out_channels // 2,
                kernel_size=kernel_size,
            )
            self.conv2 = SeparableConv2d(
                in_channels=out_channels // 2,
                out_channels=out_channels,
                kernel_size=kernel_size,
            )
        else:
            self.conv1 = torch.nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels // 2,
                kernel_size=kernel_size,
            )
            self.conv2 = torch.nn.Conv2d(
                in_channels=out_channels // 2,
                out_channels=out_channels,
                kernel_size=kernel_size,
            )

        self.bn1 = torch.nn.BatchNorm2d(out_channels // 2)
        self.bn2 = torch.nn.BatchNorm2d(out_channels)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.conv2(x)
        x = self.bn2(x)
        return x

    def fuse_layers(self):
        if not self.separable:
            modules_to_fuse = [["conv1", "bn1"], ["conv2", "bn2"]]
            for modules in modules_to_fuse:
                torch.quantization.fuse_modules(self, modules, inplace=True)
        else:
            modules_to_fuse = [["conv1.pointwise", "bn1"], ["conv2.pointwise", "bn2"]]
            for modules in modules_to_fuse:
                torch.quantization.fuse_modules(self, modules, inplace=True)


class Student(base):
    """
    A class representing a student model for knowledge distillation.

    Args:
        teacher (torch.nn.Module): The teacher model.
        student (torch.nn.Module): The student model.

    Attributes:
        teacher (torch.nn.Module): The teacher model.
        student (torch.nn.Module): The student model.
    """

    def __init__(
        self,
        teacher: torch.nn.Module,
        student: torch.nn.Module,
        classification: bool = False,
        temperature: float = 2.0,
        teacher_weight: float = 0.5,
    ):
        super(Student, self).__init__()
        self.teacher = teacher
        self.student = student
        self.classification = classification
        self.temperature = temperature
        self.teacher_weight = teacher_weight

    def forward(self, x):
        """
        Forward pass of the student model.

        Args:
            x: The input data.

        Returns:
            The output of the student model.
        """
        student_output = self.student(x)
        return student_output

    def fit_train(
        self,
        train_loader: torch.utils.data.DataLoader,
        epochs: int,
        optimizer: torch.optim.Optimizer,
        loss_fn: torch.nn.Module,
        teacher_weight: float = 0.5,
        log: bool = False,
        log_file: str = "",
        log_id: Optional[Union[int, str]] = 0,
        verbose: int = 0,
        validation_loader: Optional[torch.utils.data.DataLoader] = None,
        patience: int = 5,
    ) -> Tuple[List[float], List[float], List[float]]:
        """
        Train the student model using knowledge distillation.

        Args:
            train_loader (torch.utils.data.DataLoader): The data loader for training data.
            epochs (int): The number of training epochs.
            optimizer (torch.optim.Optimizer): The optimizer for updating model parameters.
            loss_fn (torch.nn.Module): The loss function for calculating the training loss.
            teacher_weight (float, optional): The weight for the teacher model's output in the loss calculation.
            log (bool, optional): Whether to log the training progress.
            log_file (str, optional): The file path for saving the log.
            log_id (int or str, optional): The identifier for the log.
            verbose (int, optional): The verbosity level for printing training progress.

        Returns:
            A tuple containing the losses, times and carbon emissions for each epoch.
        """

        if self.trained:
            print("Model has already been trained.")
            return self.losses, self.times, self.carbons

        if not self.teacher.trained:
            print(
                "Training teacher model for further distillation, taking the same optimizer as the student model."
            )
            teacher_optimizer = optimizer.__class__(
                self.teacher.parameters(),
                lr=optimizer.__getattribute__("param_groups")[0]["lr"],
            )
            self.teacher.fit_train(
                train_loader=train_loader,
                epochs=epochs,
                optimizer=teacher_optimizer,
                loss_fn=loss_fn,
                log=log,
                log_file=log_file,
                log_id=log_id,
                verbose=verbose,
                validation_loader=validation_loader,
                patience=patience,
            )
        self.losses, self.times, self.carbons = green_ai.utils.train_torch_kd(
            teacher=self.teacher,
            student=self.student,
            train_loader=train_loader,
            epochs=epochs,
            optimizer=optimizer,
            loss_fn=loss_fn,
            log=log,
            log_file=log_file,
            log_id=log_id,
            verbose=verbose,
            validation_loader=validation_loader,
            patience=patience,
            classification=self.classification,
            temperature=self.temperature,
            teacher_weight=self.teacher_weight,
        )

        self.losses += self.teacher.losses
        self.times += self.teacher.times
        self.carbons += self.teacher.carbons

        self.trained = True

        return self.losses, self.times, self.carbons


class FusedModel(base):
    def __init__(self, model: torch.nn.Module):
        super(FusedModel, self).__init__()
        self.base_model = model
        model = copy.deepcopy(model)

    def forward(self, x):
        return self.model(x)

    def fit_train(
        self: torch.nn.Module,
        train_loader: torch.utils.data.DataLoader,
        epochs: int,
        optimizer: torch.optim.Optimizer,
        loss_fn: torch.nn.Module,
        log: bool = False,
        log_file: str = "",
        log_id: Optional[Union[int, str]] = 0,
        verbose: int = 0,
        validation_loader: Optional[torch.utils.data.DataLoader] = None,
        patience: int = 5,
    ) -> Tuple[List[float], List[float], List[float]]:

        if self.trained:
            print("Model has already been trained.")
            return self.losses, self.times, self.carbons

        if not self.base_model.trained:
            print("Training base model for further fusion.")
            self.losses, self.times, self.carbons = green_ai.utils.train_torch(
                model=self,
                train_loader=train_loader,
                epochs=epochs,
                optimizer=optimizer,
                loss_fn=loss_fn,
                log=log,
                log_file=log_file,
                log_id=log_id,
                verbose=verbose,
                validation_loader=validation_loader,
                patience=patience,
            )
        else:
            self.losses = self.base_model.losses
            self.times = self.base_model.times
            self.carbons = self.base_model.carbons

        self.model = copy.deepcopy(self.base_model)

        self.model.eval()

        self.model.fuse_model_layers()

        self.trained = True

        return self.losses, self.times, self.carbons


class Model(base):
    def __init__(self, model: torch.nn.Module):
        super(Model, self).__init__()
        self.model = model

    def forward(self, x):
        return self.model(x)

    def fit_train(
        self: Module,
        train_loader: torch.utils.data.DataLoader,
        epochs: int,
        optimizer: Optimizer,
        loss_fn: Module,
        log: bool = False,
        log_file: str = "",
        log_id: int | str | None = 0,
        verbose: int = 0,
        validation_loader: Optional[torch.utils.data.DataLoader] = None,
        patience: int = 5,
    ) -> Tuple[List[float]]:

        if self.trained:
            print("Model has already been trained.")
            return self.losses, self.times, self.carbons

        self.losses, self.times, self.carbons = green_ai.utils.train_torch(
            model=self.model,
            train_loader=train_loader,
            epochs=epochs,
            optimizer=optimizer,
            loss_fn=loss_fn,
            log=log,
            log_file=log_file,
            log_id=log_id,
            verbose=verbose,
            validation_loader=validation_loader,
            patience=patience,
        )

        self.trained = True

        return self.losses, self.times, self.carbons
