import torch
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from typing import List, Tuple, Dict, Any, Union, Optional, Callable
import csv
import copy
import json
import sklearn.metrics
from green_ai.utils import get_model_macs
from green_ai.models import base
from green_ai.datasets import make_validation_loader

import green_ai.utils

# ----------------------------------------------------------------------------- #

# EXPERIMENTATION Class ------------------------------------------------------- #


class torch_Experimentation:
    """
    Experimentation class for experimentation with the green ai library.
    """

    def __init__(
        self,
        models: List[base],
        train_loader: torch.utils.data.DataLoader,
        test_loader: torch.utils.data.DataLoader,
        epochs: int,
        optimizers: List[torch.optim.Optimizer],
        loss_fn: torch.nn.Module,
        log_file: str,
        log_id: str,
        validation_set: bool = False,
        validation_size: float = 0.15,
        patience: int = 5,
        metric_fn: Optional[
            Union[torch.nn.Module, Callable]
        ] = sklearn.metrics.r2_score,
        model_names: List[str] = ["model1", "model2"],
        random_seed: int = 0,
        device: str = "cuda" if torch.cuda.is_available() else "cpu",
    ) -> None:
        """
        Initialize the Experimentation class.

        Args:
            models (List[torch.nn.Module]): The PyTorch models to train and test.
            train_loader (torch.utils.data.DataLoader): The DataLoader containing the training data.
            test_loader (torch.utils.data.DataLoader): The DataLoader containing the test data.
            epochs (int): The number of epochs to train for.
            optimizers (List[torch.optim.Optimizer]): The optimizers to use during training.
            metric_fn (Union[torch.nn.Module, Callable], optional): The metric function to use during testing. Defaults to sklearn.metrics.r2_score.
            model_names (List[str], optional): The names of the models. Defaults to ["model1", "model2"].
            loss_fn (torch.nn.Module): The loss function to use during training.
            log_file (str): The path to the CSV file to log training data to.
            log_id (str): The ID to associate with the training data in the log file.
        """
        self.base_models_state_dicts = [
            copy.deepcopy(model.state_dict()) for model in models
        ]
        self.models = models
        self.train_loader = train_loader
        self.valid_loader = None
        if validation_set:
            self.train_loader, self.valid_loader = make_validation_loader(
                train_loader, validation_size
            )
        self.test_loader = test_loader
        self.epochs = epochs
        if len(optimizers) == 1:
            for _ in range(len(models) - 1):
                optimizers.append(
                    optimizers[0].__class__(
                        models[_ + 1].parameters(),
                        lr=optimizers[0].param_groups[0]["lr"],
                    )
                )
        if len(models) != len(optimizers):
            raise ValueError("Number of models and optimizers must be the same or 1")
        self.optimizers = optimizers
        self.loss_fn = loss_fn
        self.log_file = log_file
        self.log_id = log_id
        self.model_names = model_names
        self.results = {}
        self.device = device
        self.metric_fn = metric_fn

        for models in self.models:
            models.to(self.device)

        self.random_seed = random_seed

        torch.manual_seed(random_seed)
        np.random.seed(random_seed)

        self.trained, self.tested, self.timed = False, False, False
        self.patience = patience

    def train(self, verbose: int = 1) -> None:
        """
        Train the PyTorch models and log the training process to a CSV file.
        """

        print("Training models...")

        for i, (model, model_name) in enumerate(zip(self.models, self.model_names)):

            print(f"Training {model_name}...")
            _, model_time, model_carbon = model.fit_train(
                train_loader=self.train_loader,
                validation_loader=self.valid_loader,
                patience=self.patience,
                optimizer=self.optimizers[i],
                loss_fn=self.loss_fn,
                epochs=self.epochs,
                log=True,
                log_file=self.log_file,
                log_id=self.log_id.upper() + "_" + model_name,
                verbose=verbose,
            )

            if not self.results.get(model_name):
                self.results[model_name] = {}

            self.results[model_name]["train"] = {
                "time": sum(model_time) / self.epochs,
                "carbon": sum(model_carbon) / self.epochs,
            }

            print(f"{model_name} trained successfully")

        self.trained = True

    def test(self) -> None:
        """
        Test the PyTorch models.
        """

        print("Testing models...")

        for model, model_name in zip(self.models, self.model_names):

            print(f"Testing {model_name}...")

            test_loss, test_metric = green_ai.utils.test_torch(
                model, self.test_loader, self.loss_fn, self.metric_fn
            )

            self.results[model_name]["test"] = {
                "loss": test_loss,
                "metric": test_metric,
            }

            print(f"{model_name} tested successfully")

        self.tested = True

    def time_models(self, num_iterations: int = 5, warm_up: int = 2) -> None:
        """
        Time the PyTorch models.
        """

        print("Timing models...")
        with open(self.log_file, "a") as file:
            writer = csv.writer(file)
            writer.writerow(["id", "inference_time", "inference_carbon"])

            for model, model_name in zip(self.models, self.model_names):
                print(f"Timing {model_name}...")
                model_time, model_carbon = green_ai.utils.time_torch(
                    model=model,
                    num_iterations=num_iterations,
                    warm_up=warm_up,
                    testloader=self.test_loader,
                    device=self.device,
                )

                self.results[model_name]["inference"] = {
                    "time": model_time,
                    "carbon": model_carbon,
                }

                print(f"{model_name} timed successfully")

                writer.writerow(
                    [self.log_id.upper() + "_" + model_name, model_time, model_carbon]
                )

        self.timed = True

    def status(self) -> None:
        """
        Print the status of the Experimentation class.
        """

        if not self.trained:
            print("Models have not been trained yet.")
        else:
            print("Models have been trained.")

        if not self.tested:
            print("Models have not been tested yet.")
        else:
            print("Models have been tested.")

        if not self.timed:
            print("Models have not been timed yet.")
        else:
            print("Models have been timed.")

    def set_random_seed(self, random_seed: int) -> None:
        """
        Set the random seed for the Experimentation class.

        Args:
            random_seed (int): The random seed to set.
        """

        self.random_seed = random_seed

        torch.manual_seed(random_seed)
        np.random.seed(random_seed)

    def print_models_summary(self) -> dict:
        """
        Print a summary of the trained and tested models.

        Returns:
            None
        """

        for model, model_name in zip(self.models, self.model_names):

            print(f"Summary for {model_name}:")
            print(model)
            print()

    def save_models(self, models_path: str) -> None:
        """
        Save the PyTorch models to disk.

        Args:
            models_path (str): The path to save the models to.
        """
        for model, model_name in zip(self.models, self.model_names):
            torch.save(model, models_path + model_name + ".pt")

    def print_results(self) -> pd.DataFrame:
        """
        Print the results of the experimentation with a dataframe comparing both models.

        Returns:
            pd.DataFrame: A dataframe containing the results of the experimentation.
        """

        if not self.results[self.model_names[0]]["inference"]:
            raise ValueError(
                "No results to print, please run the whole experiment first"
            )

        columns = [
            "Model",
            "Train Time (s)",
            "Train Carbon (gCO2eq)",
            "Test Loss",
            "Test Metric",
            "Inference Time (1000 samples)",
            "Inference Carbon (gCO2eq)",
        ]
        data = []

        for model in self.results:
            train_time = self.results[model]["train"]["time"]
            train_carbon = self.results[model]["train"]["carbon"]
            test_loss = self.results[model]["test"]["loss"]
            test_metric = self.results[model]["test"]["metric"]
            inference_time = self.results[model]["inference"]["time"]
            inference_carbon = self.results[model]["inference"]["carbon"]

            data.append(
                [
                    model,
                    train_time,
                    train_carbon,
                    test_loss,
                    test_metric,
                    inference_time,
                    inference_carbon,
                ]
            )

        df = pd.DataFrame(data, columns=columns)
        print(df)

        return df

    def plot_inference_vs_score(self, option: str = "time") -> None:
        """
        Plot the relationship between inference time or carbon emissions and test metric.

        Args:
            option (str): The option to choose between 'time' or 'carbon'. Default is 'time'.

        Raises:
            ValueError: If results are not present for models, please run the train, test, and time_models methods.
            ValueError: If the option is invalid, must be 'time' or 'carbon'.

        Returns:
            None
        """
        if (
            not self.results[self.model_names[0]]["inference"]
            or not self.results[self.model_names[0]]["train"]
            or not self.results[self.model_names[0]]["test"]
        ):
            raise ValueError(
                "Results not present for models, please run the train, test, and time_models methods"
            )

        fig, ax = plt.subplots()
        match option:
            case "time":
                x = [
                    self.results[model]["inference"]["time"]
                    for model in self.model_names
                ]
                y = [
                    self.results[model]["test"]["metric"] for model in self.model_names
                ]
                scatter = plt.scatter(
                    x,
                    y,
                    c=[
                        self.results[model]["train"]["time"]
                        for model in self.model_names
                    ],
                    cmap="RdYlGn_r",
                )
                plt.colorbar(scatter, label="Training Time (s)")
                plt.xlabel("Inference Time (1000 samples)")
                plt.title("Inference Time vs Test Metric")
            case "carbon":
                x = [
                    self.results[model]["inference"]["carbon"]
                    for model in self.model_names
                ]
                y = [
                    self.results[model]["test"]["metric"] for model in self.model_names
                ]
                scatter = plt.scatter(
                    x,
                    y,
                    c=[
                        self.results[model]["train"]["carbon"]
                        for model in self.model_names
                    ],
                    cmap="RdYlGn_r",
                )
                plt.colorbar(scatter, label="Training Carbon (gCO2eq)")
                plt.xlabel("Inference Emissions (gCO2eq)")
                plt.title("Inference Emissions vs Test Metric")
            case _:
                raise ValueError('Invalid option, must be "time" or "carbon"')

        # annotate points
        for i, txt in enumerate(self.model_names):
            ax.annotate(txt, (scatter.get_offsets()[i][0], scatter.get_offsets()[i][1]))
        plt.ylabel("Test Metric")
        plt.show()

    def plot_loss(self) -> None:
        """
        Plot the loss of the models during training.

        Raises:
            ValueError: If results are not present for models, please run train method.

        Returns:
            None
        """
        for model in self.models:
            if model.losses == []:
                raise ValueError(
                    "Results not present for models, please run the experiment"
                )

        fig, ax = plt.subplots()
        for model, model_name in zip(self.models, self.model_names):
            ax.plot(model.losses, label=model_name)
        plt.xlabel("Epoch")
        plt.ylabel("Loss")
        plt.title("Loss vs Epoch")
        plt.legend()
        plt.show()

    def plot_time(self, option: str = "time") -> None:
        """
        Plot the time or carbon emissions of the models during training.

        Args:
            option (str): The option to choose between 'time' or 'carbon'. Default is 'time'.

        Raises:
            ValueError: If results are not present for models, please run train method.

        Returns:
            None
        """

        for model in self.models:
            if model.times == []:
                raise ValueError(
                    "Results not present for models, please run the experiment"
                )

        fig, ax = plt.subplots()
        match option:
            case "time":
                for model, model_name in zip(self.models, self.model_names):
                    ax.plot(np.cumsum(model.times), label=model_name)
                plt.ylabel("Time (s)")
                plt.title("Time vs Epoch")
            case "carbon":
                for model, model_name in zip(self.models, self.model_names):
                    ax.plot(np.cumsum(model.carbons), label=model_name)
                plt.ylabel("Carbon Emissions (gCO2e)")
                plt.title("Carbon Emissions vs Epoch")
            case _:
                raise ValueError('Invalid option, must be "time" or "carbon"')
        plt.xlabel("Epoch")
        plt.legend()
        plt.show()

    def run_experiment(self, verbose: int = 1) -> None:
        """
        Run the entire experimentation process.

        Args:
            verbose (int): The verbosity level of the training process. Default is 1.
        """

        if not self.trained:
            self.train(verbose=verbose)
        if not self.tested:
            self.test()
        if not self.timed:
            self.time_models()

    def reset_status(
        self, trained: bool = False, tested: bool = False, timed: bool = False
    ) -> None:
        """
        Reset the status of the Experimentation class.

        Args:
            trained (bool): Whether the models have been trained. Default is False.
            tested (bool): Whether the models have been tested. Default is False.
            timed (bool): Whether the models have been timed. Default is False.
        """

        if trained:
            self.trained = False
            self.tested = False
            self.timed = False
            # set models params to initial state
            for j, model in enumerate(self.models):
                model.load_state_dict(self.base_models_state_dicts[j])

            self.results = {}
            for model in self.models:
                model.to(self.device)
        if tested:
            self.tested = False
        if timed:
            self.timed = False

    def save_results(self, path: str) -> None:
        """
        Save the results of the Experimentation class to disk.

        Args:
            path (str): The path to save the results to.
        """

        with open(path, "w") as file:
            json.dump(self.results, file)

    def get_results(self) -> dict:
        """
        Get the results of the Experimentation class.

        Returns:
            dict: A dictionary containing the results of the Experimentation class.
        """

        return self.results

    def get_models(self) -> Tuple[torch.nn.Module, torch.nn.Module]:
        """
        Get the models trained in the Experimentation class.

        Returns:
            tuple: A tuple containing the models.
        """

        return tuple(self.models)

    def get_model_sizes(self) -> Dict[str, int]:
        """
        Get the sizes of the models trained in the Experimentation class.

        Returns:
            Dict[str, int]: A dictionary containing the sizes of the models.
        """
        dict = {}
        for i, model in enumerate(self.models):
            dict[self.model_names[i]] = green_ai.utils.get_model_size(model)

        return dict

    def get_models_macs(self) -> Dict[str, int]:
        """
        Get the MACs of the models trained in the Experimentation class.

        Returns:
            Dict[str, int]: A dictionary containing the MACs of the models.
        """
        dict = {}
        input_shape = self.train_loader.dataset[0][0].shape
        for i, model in enumerate(self.models):
            dict[self.model_names[i]] = green_ai.utils.get_model_macs(
                model, input_shape
            )

        return dict
