import sklearn.linear_model
import sklearn.ensemble
import sklearn.neural_network
import sklearn.svm
import argparse
import logging
import os
import sys
import time
import warnings
import pandas as pd

from sklearn.preprocessing import StandardScaler
from sklearn.utils import shuffle

from codecarbon import EmissionsTracker
from IPython.utils import io
from pytorch_lightning import LightningModule, Trainer
from pytorch_lightning import loggers as pl_loggers
from torch.utils.data import DataLoader

warnings.filterwarnings("ignore")  # Disable data logger warnings
logging.getLogger("pytorch_lightning").setLevel(logging.ERROR)  # Disable GPU/TPU prints

DATA_PATH = "/Users/camille.hascoet/Documents/greenai/src/hale/sandbox/data/tracks.csv"
SEED = 42


def parse_args():
    parser = argparse.ArgumentParser(description="train mnist")
    parser.add_argument(
        "--log_path",
        type=str,
        required=True,
        help="dir to place tensorboard logs from all trials",
    )
    parser.add_argument(
        "--ml_model", type=str, required=True, help="Machine learning model to use"
    )
    return parser.parse_args()


args = parse_args()


def make_ml_model(ml_model: str):
    if ml_model == "linear":
        return sklearn.linear_model.LinearRegression()
    elif ml_model == "ridge":
        return sklearn.linear_model.Ridge()
    elif ml_model == "lasso":
        return sklearn.linear_model.Lasso()
    elif ml_model == "elastic":
        return sklearn.linear_model.ElasticNet()
    elif ml_model == "rf":
        return sklearn.ensemble.RandomForestRegressor()
    elif ml_model == "gb":
        return sklearn.ensemble.GradientBoostingRegressor()
    elif ml_model == "mlp":
        return sklearn.neural_network.MLPRegressor()
    elif ml_model == "svr":
        return sklearn.svm.SVR()
    else:
        raise ValueError(f"Unknown model {ml_model}")


def get_data(path: str = DATA_PATH):
    df = pd.read_csv(path)
    X = df.drop(
        ["popularity", "id", "name", "artists", "id_artists", "release_date"], axis=1
    )
    y = df["popularity"]
    X_drop = X.drop(
        ["mode", "valence", "key", "duration_ms", "speechiness", "liveness"], axis=1
    )
    X.drop = X.drop(
        [
            "loudness",
            "energy",
            "explicit",
            "acousticness",
            "instrumentalness",
            "danceability",
        ],
        axis=1,
    )

    X_scaler = StandardScaler()
    X_scaled = X_scaler.fit_transform(X_drop)
    y_scaled = y.values.reshape(-1, 1) / 100

    X_shuffled, y_shuffled = shuffle(X_scaled, y_scaled, random_state=SEED)

    X_train = X_shuffled[:20000]
    y_train = y_shuffled[:20000]

    X_test = X_shuffled[20000:25000]
    y_test = y_shuffled[20000:25000]

    return X_train, y_train, X_test, y_test


def train_ml_model(ml_model, X_train, y_train, X_test, y_test):
    ml_model.fit(X_train, y_train)
    score = sklearn.metrics.mean_absolute_error(y_test, ml_model.predict(X_test))
    return score


def get_ml_model_inference_emissions(ml_model, X_test, warm_up_steps=3, num_steps=5):
    tracker = EmissionsTracker(log_level="critical", save_to_file=False)
    for _ in range(warm_up_steps):
        ml_model.predict(X_test)
    tracker.start()
    for _ in range(num_steps):
        ml_model.predict(X_test)
    emissions = tracker.stop() * 1000 / (num_steps * len(X_test))
    return emissions


def run_training_job():
    logger = pl_loggers.TensorBoardLogger(args.log_path)
    ml_model = make_ml_model(args.ml_model)
    X_train, y_train, X_test, y_test = get_data()
    score = train_ml_model(ml_model, X_train, y_train, X_test, y_test)
    emissions = get_ml_model_inference_emissions(ml_model, X_test)
    logger.log_metrics({"val_mae": score})
    logger.log_metrics({"model_inference_carbon": emissions})
    logger.save()

    print(
        f"Model {args.ml_model} trained with score {score} and inference emissions {emissions}"
    )


if __name__ == "__main__":
    run_training_job()
