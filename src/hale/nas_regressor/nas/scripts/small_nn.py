import argparse
import logging
import time
import warnings
import numpy as np
import os
from pathlib import Path

from codecarbon import EmissionsTracker
import torch
from IPython.utils import io
from pytorch_lightning import LightningModule, Trainer
from pytorch_lightning import loggers as pl_loggers
from torch import nn
from torch.nn import functional as F
from torch.utils.data import DataLoader
from torchmetrics.functional.classification.accuracy import multiclass_accuracy

warnings.filterwarnings("ignore")  # Disable data logger warnings
logging.getLogger("pytorch_lightning").setLevel(logging.ERROR)  # Disable GPU/TPU prints


def parse_args():
    parser = argparse.ArgumentParser(description="train small_nn")
    parser.add_argument(
        "--log_path",
        type=str,
        required=True,
        help="dir to place tensorboard logs from all trials",
    )
    parser.add_argument(
        "--hidden_size_1", type=int, required=True, help="hidden size layer 1"
    )
    parser.add_argument(
        "--hidden_size_2", type=int, required=True, help="hidden size layer 2"
    )
    parser.add_argument(
        "--learning_rate", type=float, required=True, help="learning rate"
    )
    parser.add_argument("--epochs", type=int, default=5, help="number of epochs")
    parser.add_argument(
        "--dropout", type=float, required=True, help="dropout probability"
    )
    parser.add_argument("--batch_size", type=int, default=32, help="batch size")
    return parser.parse_args()


args = parse_args()

PATH_DATASETS = "tmp/"


class small_NN(LightningModule):
    def __init__(self):
        super().__init__()
        self.prepare_data()
        # Tunable parameters
        self.hidden_size_1 = args.hidden_size_1
        self.hidden_size_2 = args.hidden_size_2
        self.learning_rate = args.learning_rate
        self.dropout = args.dropout
        self.batch_size = args.batch_size

        # Set class attributes
        self.data_dir = PATH_DATASETS

        # Create a PyTorch model
        layers = [nn.Flatten()]
        hidden_layers = [self.hidden_size_1, self.hidden_size_2]
        num_params = 0
        self.input_size = self.val_dataloader().dataset[0][0].shape[0]
        width = self.input_size
        for hidden_size in hidden_layers:
            if hidden_size > 0:
                layers.append(nn.Linear(width, hidden_size))
                layers.append(nn.ReLU())
                layers.append(nn.Dropout(self.dropout))
                num_params += width * hidden_size
                width = hidden_size
        layers.append(nn.Linear(width, 1))
        num_params += width * 1

        # Save the model and parameter counts
        self.num_params = num_params
        self.model = nn.Sequential(*layers)  # No need to use Relu for the last layer

    def forward(self, x):
        x = self.model(x)
        return x

    def training_step(self, batch, batch_idx):
        x, y = batch
        pred = self(x)
        loss = F.l1_loss(pred, y)
        return loss

    def validation_step(self, batch, batch_idx):
        x, y = batch
        pred = self(x)
        loss = F.l1_loss(pred, y)
        self.log("mae", loss, prog_bar=False)
        return loss

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.learning_rate)
        return optimizer

    def prepare_data(self):
        self.X_train = np.load(os.path.join(PATH_DATASETS, "X_train.npy"))
        self.X_test = np.load(os.path.join(PATH_DATASETS, "X_test.npy"))
        self.y_train = np.load(os.path.join(PATH_DATASETS, "y_train.npy"))
        self.y_test = np.load(os.path.join(PATH_DATASETS, "y_test.npy"))
        # convert to tensor
        self.X_train = torch.tensor(self.X_train, dtype=torch.float32)
        self.X_test = torch.tensor(self.X_test, dtype=torch.float32)
        self.y_train = torch.tensor(self.y_train, dtype=torch.float32)
        self.y_test = torch.tensor(self.y_test, dtype=torch.float32)

    def train_dataloader(self):
        return DataLoader(
            list(zip(self.X_train, self.y_train)),
            batch_size=self.batch_size,
            shuffle=True,
        )

    def val_dataloader(self):
        return DataLoader(
            list(zip(self.X_test, self.y_test)),
            batch_size=self.batch_size,
            shuffle=False,
        )

    def get_inference_carbon(self, warmup_steps=20, num_steps=100):
        tracker = EmissionsTracker(log_level="critical", save_to_file=False)
        with torch.no_grad():
            for _ in range(warmup_steps):
                self(torch.rand(10, self.input_size))
            tracker.start()
            for _ in range(num_steps):
                self(torch.rand(10, self.input_size))
            emissions = tracker.stop() * 1000 / (num_steps * 10)
        return emissions


def run_training_job():

    model = small_NN()

    # Initialize a trainer (don't log anything since things get so slow...)
    trainer = Trainer(
        logger=False,
        max_epochs=args.epochs,
        enable_progress_bar=False,
        deterministic=True,  # Do we want a bit of noise?
        default_root_dir=args.log_path,
    )

    logger = pl_loggers.TensorBoardLogger(args.log_path)

    print(f"Logging to path: {args.log_path}.")

    # Train the model and log time and emissions
    start = time.time()
    tracker = EmissionsTracker()
    tracker.start()
    trainer.fit(model=model)
    emissions = tracker.stop() * 1000
    end = time.time()
    train_time = end - start
    logger.log_metrics({"training_time": end - start})
    logger.log_metrics({"train_emissions": emissions})

    # Compute the validation accuracy once and log the score
    with io.capture_output() as captured:
        mae = trainer.validate()[0]["mae"]
    logger.log_metrics({"mae": mae})

    # Log the number of model parameters
    num_params = trainer.model.num_params
    logger.log_metrics({"num_params": num_params})

    logger.log_metrics({"inference_emissions": model.get_inference_carbon()})

    logger.save()

    # Print outputs
    print(f"train time: {train_time}, val mae: {mae}, num_params: {num_params}")


if __name__ == "__main__":
    run_training_job()
