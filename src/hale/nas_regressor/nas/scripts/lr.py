import sklearn.linear_model
import argparse
import logging
import os
import warnings
import numpy as np


from codecarbon import EmissionsTracker
from pytorch_lightning import loggers as pl_loggers

warnings.filterwarnings("ignore")  # Disable data logger warnings
logging.getLogger("pytorch_lightning").setLevel(logging.ERROR)  # Disable GPU/TPU prints

DATA_PATH = "tmp/"
SEED = 42


def parse_args():
    parser = argparse.ArgumentParser(description="train mnist")
    parser.add_argument(
        "--log_path",
        type=str,
        required=True,
        help="dir to place tensorboard logs from all trials",
    )
    return parser.parse_known_args()


args, unknown = parse_args()

args_dict = {}
for i in range(0, len(unknown), 2):
    key = unknown[i].lstrip("-")  # Remove leading dashes
    try:
        value = float(unknown[i + 1])
        if value == int(value):
            value = int(value)
    except ValueError:
        value = unknown[i + 1]
    args_dict[key] = value

bool_keys = ["fit_intercept"]
for key in bool_keys:
    if key in args_dict:
        args_dict[key] = True if args_dict[key] else False


def get_data(path: str = DATA_PATH):

    X_train = np.load(os.path.join(path, "X_train.npy"))
    X_test = np.load(os.path.join(path, "X_test.npy"))
    y_train = np.load(os.path.join(path, "y_train.npy"))
    y_test = np.load(os.path.join(path, "y_test.npy"))

    return X_train, y_train, X_test, y_test


def train_ml_model(ml_model, X_train, y_train):
    tracker = EmissionsTracker(log_level="critical", save_to_file=False)
    tracker.start()
    ml_model.fit(X_train, y_train)
    train_emissions = tracker.stop() * 1000
    return train_emissions


def score_ml_model(ml_model, X_test, y_test, metric):
    y_pred = ml_model.predict(X_test)
    return metric(y_test, y_pred)


def get_ml_model_inference_emissions(ml_model, X_test, warm_up_steps=3, num_steps=5):
    tracker = EmissionsTracker(log_level="critical", save_to_file=False)
    for _ in range(warm_up_steps):
        ml_model.predict(X_test)
    tracker.start()
    for _ in range(num_steps):
        ml_model.predict(X_test)
    emissions = tracker.stop() * 1000 / (num_steps * len(X_test))
    return emissions


def run_training_job():
    logger = pl_loggers.TensorBoardLogger(args.log_path)
    ml_model = sklearn.linear_model.LinearRegression(**args_dict)
    X_train, y_train, X_test, y_test = get_data()
    train_emissions = train_ml_model(ml_model, X_train, y_train)
    logger.log_metrics({"train_emissions": train_emissions})
    score = score_ml_model(
        ml_model, X_test, y_test, sklearn.metrics.mean_absolute_error
    )
    emissions = get_ml_model_inference_emissions(ml_model, X_test)
    logger.log_metrics({"mae": score})
    logger.log_metrics({"inference_emissions": emissions})
    logger.save()

    print(f"LR Model trained with score {score} and inference emissions {emissions}")


if __name__ == "__main__":
    run_training_job()
