# ------------------------------ IMPORTS ------------------------------ #

from ax.core import (
    ChoiceParameter,
    ParameterType,
    RangeParameter,
    SearchSpace,
    FixedParameter,
)

from ax.core import OrderConstraint

from typing import List, Optional

from torchx.components import utils

from pathlib import Path

import torchx
from torchx import specs

from ax.metrics.tensorboard import TensorboardCurveMetric
from ax.core import MultiObjective, Objective, ObjectiveThreshold
from ax.core.optimization_config import MultiObjectiveOptimizationConfig

# ------------------------------ DEFAULT ------------------------------ #


def get_default_search_space(search_space_name: str) -> SearchSpace:
    """
    Get the default search space for a given search space name.

    Parameters:
        search_space_name (str): The name of the search space.

    Returns:
        SearchSpace: The default search space corresponding to the given search space name.

    Raises:
        ValueError: If the default search space for the given search space name is not implemented.
    """
    match search_space_name:
        case "rf":  # random forest
            return SearchSpace(
                parameters=[
                    RangeParameter(
                        name="n_estimators",
                        parameter_type=ParameterType.INT,
                        lower=10,
                        upper=100,
                    ),
                    RangeParameter(
                        name="max_depth",
                        parameter_type=ParameterType.INT,
                        lower=1,
                        upper=10,
                    ),
                    ChoiceParameter(
                        name="criterion",
                        parameter_type=ParameterType.STRING,
                        values=["squared_error", "friedman_mse"],
                    ),
                ]
            )
        case "lr":  # linear regression
            return SearchSpace(
                parameters=[
                    RangeParameter(
                        name="fit_intercept",
                        parameter_type=ParameterType.INT,
                        lower=0,
                        upper=1,
                    ),
                ]
            )
        case "svr":  # support vector regression
            return SearchSpace(
                parameters=[
                    RangeParameter(
                        name="C",
                        parameter_type=ParameterType.FLOAT,
                        lower=0.1,
                        upper=10.0,
                    ),
                    RangeParameter(
                        name="epsilon",
                        parameter_type=ParameterType.FLOAT,
                        lower=0.1,
                        upper=1.0,
                    ),
                    ChoiceParameter(
                        name="kernel",
                        parameter_type=ParameterType.STRING,
                        values=["linear", "rbf", "sigmoid"],
                    ),
                ]
            )
        case "mlp":  # multi-layer perceptron
            return SearchSpace(
                parameters=[
                    RangeParameter(
                        name="hidden_layer_sizes",
                        parameter_type=ParameterType.INT,
                        lower=10,
                        upper=100,
                    ),
                    ChoiceParameter(
                        name="activation",
                        parameter_type=ParameterType.STRING,
                        values=["identity", "logistic", "tanh", "relu"],
                    ),
                    RangeParameter(
                        name="hidden_layer_number",
                        parameter_type=ParameterType.INT,
                        lower=1,
                        upper=5,
                    ),
                ]
            )
        case "knn":  # k-nearest neighbors
            return SearchSpace(
                parameters=[
                    RangeParameter(
                        name="n_neighbors",
                        parameter_type=ParameterType.INT,
                        lower=1,
                        upper=10,
                    ),
                    ChoiceParameter(
                        name="weights",
                        parameter_type=ParameterType.STRING,
                        values=["uniform", "distance"],
                    ),
                    ChoiceParameter(
                        name="algorithm",
                        parameter_type=ParameterType.STRING,
                        values=["auto", "kd_tree", "brute"],
                    ),
                ]
            )
        case "dt":  # decision tree
            return SearchSpace(
                parameters=[
                    RangeParameter(
                        name="max_depth",
                        parameter_type=ParameterType.INT,
                        lower=1,
                        upper=10,
                    ),
                    RangeParameter(
                        name="min_samples_split",
                        parameter_type=ParameterType.INT,
                        lower=2,
                        upper=10,
                    ),
                    ChoiceParameter(
                        name="criterion",
                        parameter_type=ParameterType.STRING,
                        values=["poisson", "friedman_mse"],
                    ),
                ]
            )
        case "xgb":  # xgboost
            return SearchSpace(
                parameters=[
                    RangeParameter(
                        name="n_estimators",
                        parameter_type=ParameterType.INT,
                        lower=10,
                        upper=100,
                    ),
                    RangeParameter(
                        name="max_depth",
                        parameter_type=ParameterType.INT,
                        lower=1,
                        upper=10,
                    ),
                    RangeParameter(
                        name="learning_rate",
                        parameter_type=ParameterType.FLOAT,
                        lower=0.01,
                        upper=0.1,
                    ),
                ]
            )
        case "small_nn":  # torch neural network
            parameters = [
                RangeParameter(
                    name="hidden_size_1",
                    parameter_type=ParameterType.INT,
                    lower=10,
                    upper=100,
                ),
                RangeParameter(
                    name="hidden_size_2",
                    parameter_type=ParameterType.INT,
                    lower=10,
                    upper=100,
                ),
                RangeParameter(
                    name="learning_rate",
                    parameter_type=ParameterType.FLOAT,
                    lower=0.01,
                    upper=0.1,
                ),
                RangeParameter(
                    name="dropout",
                    parameter_type=ParameterType.FLOAT,
                    lower=0,
                    upper=0.3,
                ),
            ]
            return SearchSpace(
                parameters=parameters,
                parameter_constraints=[
                    OrderConstraint(
                        lower_parameter=parameters[1], upper_parameter=parameters[0]
                    )
                ],
            )
        case _:
            raise ValueError(
                f"Default search space {search_space_name} not implemented"
            )


def add_script_parameter(search_space: SearchSpace, name: str) -> SearchSpace:
    """
    Add the script parameter to the given search space.

    Args:
        search_space (SearchSpace): The search space to which to add the script parameter.
        script (str): The name of the script to add as a parameter.

    Returns the search space with the added script parameter.

    """
    search_space.parameters["script"] = FixedParameter(
        name="script", parameter_type=ParameterType.STRING, value=name + ".py"
    )


def trainer(trial_idx: int = -1, **kwargs) -> specs.AppDef:
    """
    A generic trainer function that can be used to train any model.

    Args:
        **kwargs: The keyword arguments to pass to the trainer function.

    Returns:
        specs.AppDef: The TorchX ``AppDef`` for the trainer function.
    """
    log_path = kwargs.get("log_path", "")
    if trial_idx >= 0:
        log_path = Path(log_path).joinpath(str(trial_idx)).absolute().as_posix()

    args = []
    for key, value in kwargs.items():
        if key == "script" or key == "log_path":
            continue
        args.append(f"--{key}")
        args.append(str(value))

    return utils.python(
        "--log_path",
        log_path,
        *args,
        name="trainer",
        script="nas/scripts/" + kwargs["script"],
        image=torchx.version.TORCHX_IMAGE,
    )


class MyTensorboardMetric(TensorboardCurveMetric):

    def __init__(self, name: str, curve_name: str, lower_is_better: bool, path: str):
        super().__init__(
            name=name, curve_name=curve_name, lower_is_better=lower_is_better
        )
        self.path = path

    def get_ids_from_trials(self, trials):
        return {
            trial.index: Path(self.path).joinpath(str(trial.index)).as_posix()
            for trial in trials
        }

    @classmethod
    def is_available_while_running(cls):
        return False


def setup_objectives(
    metrics: List[str], path: str, metrics_bounds: Optional[List[float]] = None
) -> MultiObjectiveOptimizationConfig:
    """
    Set up the objectives for multi-objective optimization.

    Args:
        metrics (List[str]): A list of metric names.
        path (str): The path to the metrics.
        metrics_bounds (Optional[List[float]], optional): A list of metric bounds. Defaults to None.

    Returns:
        MultiObjectiveOptimizationConfig: The configuration for multi-objective optimization.

    Raises:
        ValueError: If a metric is not implemented or if the lengths of metrics and metrics_bounds do not match.

    """
    for metric in metrics:
        if metric not in ["mae", "inference_emissions", "mse", "train_emissions"]:
            raise ValueError(
                f"Metric {metric} not implemented, please use one of ['mae', 'inference_emissions', 'mse', 'train_emissions']"
            )
    objectives = []
    objectives_tresholds = []
    if metrics_bounds is None:
        for metric in metrics:
            if metric == "mae":
                objectives.append(
                    Objective(
                        metric=MyTensorboardMetric(
                            name="mae",
                            curve_name="mae",
                            lower_is_better=True,
                            path=path,
                        ),
                        minimize=True,
                    )
                )
            elif metric == "inference_emissions":
                objectives.append(
                    Objective(
                        metric=MyTensorboardMetric(
                            name="inference_emissions",
                            curve_name="inference_emissions",
                            lower_is_better=True,
                            path=path,
                        ),
                        minimize=True,
                    )
                )
            elif metric == "mse":
                objectives.append(
                    Objective(
                        metric=MyTensorboardMetric(
                            name="mse",
                            curve_name="mse",
                            lower_is_better=True,
                            path=path,
                        ),
                        minimize=True,
                    )
                )
            elif metric == "train_emissions":
                objectives.append(
                    Objective(
                        metric=MyTensorboardMetric(
                            name="train_emissions",
                            curve_name="train_emissions",
                            lower_is_better=True,
                            path=path,
                        ),
                        minimize=True,
                    )
                )
    else:
        assert len(metrics) == len(
            metrics_bounds
        ), "metrics and metrics_bounds must have the same length"
        for metric, bound in zip(metrics, metrics_bounds):
            if metric == "mae":
                mae = MyTensorboardMetric(
                    name="mae", curve_name="mae", lower_is_better=True, path=path
                )
                objectives.append(Objective(metric=mae, minimize=True))
                objectives_tresholds.append(
                    ObjectiveThreshold(metric=mae, bound=bound, relative=False)
                )
            elif metric == "inference_emissions":
                inference_emissions = MyTensorboardMetric(
                    name="inference_emissions",
                    curve_name="inference_emissions",
                    lower_is_better=True,
                    path=path,
                )
                objectives.append(Objective(metric=inference_emissions, minimize=True))
                objectives_tresholds.append(
                    ObjectiveThreshold(
                        metric=inference_emissions, bound=bound, relative=False
                    )
                )
            elif metric == "mse":
                mse = MyTensorboardMetric(
                    name="mse", curve_name="mse", lower_is_better=True, path=path
                )
                objectives.append(Objective(metric=mse, minimize=True))
                objectives_tresholds.append(
                    ObjectiveThreshold(metric=mse, bound=bound, relative=False)
                )
            elif metric == "train_emissions":
                train_emissions = MyTensorboardMetric(
                    name="train_emissions",
                    curve_name="train_emissions",
                    lower_is_better=True,
                    path=path,
                )
                objectives.append(Objective(metric=train_emissions, minimize=True))
                objectives_tresholds.append(
                    ObjectiveThreshold(
                        metric=train_emissions, bound=bound, relative=False
                    )
                )
    return MultiObjectiveOptimizationConfig(
        objective=MultiObjective(objectives), objective_thresholds=objectives_tresholds
    )


def set_pareto_points_2d(
    points: dict, x_is_min: bool = True, y_is_min: bool = True
) -> dict:
    points_list = []
    for name in points:
        for point in points[name]:
            points_list.append([point["x"], point["y"]])

    for name in points:
        for point in points[name]:
            point["pareto"] = True
            for other_point in points_list:
                if (
                    (x_is_min and point["x"] > other_point[0])
                    or (not x_is_min and point["x"] < other_point[0])
                ) and (
                    (y_is_min and point["y"] > other_point[1])
                    or (not y_is_min and point["y"] < other_point[1])
                ):
                    point["pareto"] = False
                    break

    return points


def set_pareto_points_3d(
    points: dict, x_is_min: bool = True, y_is_min: bool = True, z_is_min: bool = True
) -> dict:
    points_list = []
    for name in points:
        for point in points[name]:
            points_list.append([point["x"], point["y"], point["z"]])

    for name in points:
        for point in points[name]:
            point["pareto"] = True
            for other_point in points_list:
                if (
                    (
                        (x_is_min and point["x"] > other_point[0])
                        or (not x_is_min and point["x"] < other_point[0])
                    )
                    and (
                        (y_is_min and point["y"] > other_point[1])
                        or (not y_is_min and point["y"] < other_point[1])
                    )
                    and (
                        (z_is_min and point["z"] > other_point[2])
                        or (not z_is_min and point["z"] < other_point[2])
                    )
                ):
                    point["pareto"] = False
                    break

    return points
