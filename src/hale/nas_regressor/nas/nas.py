from pathlib import Path
from matplotlib import pyplot as plt

import os

import numpy as np

import tempfile
from ax.runners.torchx import TorchXRunner

from ax.core import SearchSpace

from ax.core.optimization_config import MultiObjectiveOptimizationConfig

from ax.core import Experiment

from ax.modelbridge.dispatch_utils import choose_generation_strategy
from ax.service.scheduler import Scheduler, SchedulerOptions

from ax.service.utils.report_utils import exp_to_df
from ax.service.utils.report_utils import _pareto_frontier_scatter_2d_plotly

from typing import List, Optional, Dict

import nas.utils

from sklearn.model_selection import train_test_split


class NAS:
    """
    Class representing Neural Architecture Search (NAS).

    Parameters:
    - search_spaces_names (List[str]): List of names of the search spaces.
    - X (np.ndarray): Input data.
    - y (np.ndarray): Target data.
    - test_size (float, optional): The proportion of the dataset to include in the test split. Default is 0.2.
    - search_spaces (Optional[List[SearchSpace]], optional): List of search spaces. If not provided, default search spaces will be used. Default is None.
    - search_spaces_weight (Optional[List[float]], optional): List of weights for each search space. If not provided, equal weights will be assigned. Default is None.
    - num_trials (int, optional): Number of trials for each search space. Default is 32.
    - metrics (List[str], optional): List of metrics to optimize. Default is ["mae", "inference_emissions"].
    - metrics_bounds (List[float], optional): List of bounds for each metric. Default is None.
    """

    def __init__(
        self,
        search_spaces_names: List[str],
        X: np.ndarray,
        y: np.ndarray,
        test_size: float = 0.2,
        search_spaces: Optional[Dict[str, SearchSpace]] = {},
        search_spaces_weight: Optional[List[float]] = None,
        num_trials: int = 32,
        metrics: List[str] = ["mae", "inference_emissions"],
        metrics_bounds: List[float] = None,
    ):
        self.search_spaces = {name: {} for name in search_spaces_names}

        for name in search_spaces_names:
            if name not in search_spaces.keys():
                print(f"Using default search space for {name}")
                self.search_spaces[name]["search_space"] = (
                    self._get_default_search_space(name)
                )
            else:
                self.search_spaces[name]["search_space"] = search_spaces[name]

        self._check_search_spaces()
        self.num_trials = num_trials
        search_spaces_num_trials = {}

        if search_spaces_weight is None:
            search_spaces_weight = [1 / len(search_spaces_names)] * len(
                search_spaces_names
            )

        assert sum(search_spaces_weight) == 1, "search_spaces_weight must sum to 1"

        for name, weight in zip(search_spaces_names, search_spaces_weight):
            search_spaces_num_trials[name] = round(num_trials * weight)

        self.search_spaces_weight = search_spaces_weight
        self.search_spaces_num_trials = search_spaces_num_trials

        self.metrics = metrics
        self._check_metrics()
        self.metrics_bounds = metrics_bounds

        self.X = X
        self.y = y
        self.test_size = test_size

        self._split_and_save_data()

    def _split_and_save_data(self):
        """
        Split the data into train and testing sets, and save them as numpy arrays.
        """
        X_train, X_test, y_train, y_test = train_test_split(
            self.X, self.y, test_size=self.test_size
        )
        # check if tmp folder exists
        if not Path("tmp").is_dir():
            Path("tmp").mkdir()

        np.save("tmp/X_train.npy", X_train)
        np.save("tmp/X_test.npy", X_test)
        np.save("tmp/y_train.npy", y_train)
        np.save("tmp/y_test.npy", y_test)

    def run_nas(self):
        """
        Run the Neural Architecture Search (NAS) for each search space.
        """
        for name in self.search_spaces.keys():
            print(f"Running NAS for {name}")
            log_path = tempfile.mkdtemp(prefix=f"nas_{name}_")
            trainer = nas.utils.trainer
            ax_runner = self.make_runner(log_path, trainer)
            opt_config = self._get_optimization_config(path=log_path)
            search_space = self.search_spaces[name]["search_space"]
            experiment = Experiment(
                name="nas_" + name,
                search_space=search_space,
                optimization_config=opt_config,
                runner=ax_runner,
            )
            self.search_spaces[name]["experiment"] = experiment
            num_trials = self.search_spaces_num_trials[name]
            gs = choose_generation_strategy(
                search_space=experiment.search_space,
                optimization_config=experiment.optimization_config,
                num_trials=num_trials,
            )
            scheduler = Scheduler(
                experiment=experiment,
                generation_strategy=gs,
                options=SchedulerOptions(
                    total_trials=num_trials, max_pending_trials=num_trials // 8 + 1
                ),
            )
            scheduler.run_all_trials()

    def _get_optimization_config(self, path: str) -> MultiObjectiveOptimizationConfig:
        """
        Get the optimization configuration for the experiment.

        Parameters:
        - path (str): Path to the log directory.

        Returns:
        - MultiObjectiveOptimizationConfig: The optimization configuration.
        """
        opt_config = nas.utils.setup_objectives(
            metrics=self.metrics, metrics_bounds=self.metrics_bounds, path=path
        )
        return opt_config

    def make_runner(self, log_path: str, trainer: callable):
        """
        Create a runner for the experiment.

        Parameters:
        - log_path (str): Path to the log directory.
        - trainer (callable): The trainer function.

        Returns:
        - TorchXRunner: The runner for the experiment.
        """
        return TorchXRunner(
            tracker_base="/tmp/",
            component=trainer,
            scheduler="local_cwd",
            component_const_params={"log_path": log_path},
            cfg={},
        )

    def _check_search_spaces(self):
        """
        Check if the search space scripts exist for each search space.
        """
        for name in self.search_spaces.keys():
            if not Path(f"nas/scripts/{name}.py").is_file():
                raise ValueError(
                    f"Search space {name} not implemented, please implement it at nas/nas/scripts/{name}.py"
                )
            nas.utils.add_script_parameter(
                self.search_spaces[name]["search_space"], name
            )

    def _get_default_search_space(self, search_space_name: str) -> SearchSpace:
        """
        Get the default search space for the given search space name.

        Parameters:
        - search_space_name (str): Name of the search space.

        Returns:
        - SearchSpace: The default search space.
        """
        return nas.utils.get_default_search_space(search_space_name)

    def _check_metrics(self):
        """
        Check if the metrics are valid.
        """
        for metric in self.metrics:
            if metric not in [
                "mae",
                "inference_emissions",
                "mse",
                "train_emissions",
            ]:
                raise ValueError(
                    f"Metric {metric} not implemented, please use one of ['mae', 'inference_emissions', 'mse', 'train_emissions']"
                )
        if len(self.metrics) not in [2, 3]:
            raise ValueError("Number of metrics must be 2 or 3")

    def remove_tmp_files(self):
        """
        Remove temporary files.
        """
        for file in Path("tmp").glob("*"):
            file.unlink()

        os.rmdir("tmp")

    def exp_to_df(self, name: str):
        """
        Convert the experiment results to a pandas DataFrame.

        Parameters:
        - name (str): Name of the search space.

        Returns:
        - pd.DataFrame: The experiment results as a DataFrame.
        """
        return exp_to_df(self.search_spaces[name]["experiment"])

    def pareto_frontier_scatter_2d_plotly(self, name: str):
        """
        Generate a 2D scatter plot of the Pareto frontier using Plotly.

        Parameters:
        - name (str): Name of the search space.

        Returns:
        - plotly.graph_objects.Figure: The Plotly figure.
        """
        return _pareto_frontier_scatter_2d_plotly(
            self.search_spaces[name]["experiment"]
        )

    def _retrieve_all_points(self):
        """
        Retrieve all points from the experiments.

        Returns:
        - dict: Dictionary containing all points from the experiments.
        """

        all_points = {}
        for name in self.search_spaces.keys():
            search_space_points = self._retrieve_points(name)
            if len(self.metrics) == 2:
                all_points[name] = [
                    {"x": point[0], "y": point[1], "pareto": None}
                    for point in search_space_points
                ]
            elif len(self.metrics) == 3:
                all_points[name] = [
                    {"x": point[0], "y": point[1], "z": point[2], "pareto": None}
                    for point in search_space_points
                ]
            else:
                raise ValueError(
                    "Pareto dictionary only implemented for 2 or 3 metrics"
                )

        return all_points

    def _retrieve_points(self, name: str):
        """
        Retrieve the points from the experiment.

        Parameters:
        - name (str): Name of the search space.

        Returns:
        - list: List of points but only their metrics values.
        """
        df = exp_to_df(self.search_spaces[name]["experiment"])
        return df[self.metrics].values.tolist()

    def new_trial(self, name: str):
        """
        Add a new trial to the experiment.

        Parameters:
        - name (str): Name of the search space.
        """
        self.search_spaces[name]["experiment"].new_trial()

    def get_pareto_dict(self):
        """
        Get the Pareto dictionary.

        Returns:
        - dict: Dictionary containing the Pareto points.
        """
        all_points = self._retrieve_all_points()
        x_is_min = (
            True
            if self.metrics[0]
            in ["mae", "inference_emissions", "mse", "train_emissions"]
            else False
        )
        y_is_min = (
            True
            if self.metrics[1]
            in ["mae", "inference_emissions", "mse", "train_emissions"]
            else False
        )
        if len(self.metrics) == 2:
            all_points = nas.utils.set_pareto_points_2d(all_points, x_is_min, y_is_min)
        elif len(self.metrics) == 3:
            z_is_min = (
                True
                if self.metrics[2]
                in ["mae", "inference_emissions", "mse", "train_emissions"]
                else False
            )
            all_points = nas.utils.set_pareto_points_3d(
                all_points, x_is_min, y_is_min, z_is_min
            )
        else:
            raise ValueError("Pareto dictionary only implemented for 2 or 3 metrics")
        self.pareto_dict = all_points
        return all_points

    def plot_pareto(self):
        """
        Plot the Pareto frontier.

        Returns:
        - plotly.graph_objects.Figure: The Plotly figure.
        """
        if len(self.metrics) == 2:
            self._plot_pareto_2d()
        elif len(self.metrics) == 3:
            self._plot_pareto_3d()
        else:
            raise ValueError("Pareto plot only implemented for 2 or 3 metrics")

    def _plot_pareto_2d(self):
        """
        Plot the Pareto frontier.

        Returns:
        - plotly.graph_objects.Figure: The Plotly figure.
        """
        all_points = self.get_pareto_dict()
        plt.figure(figsize=(10, 10))
        for name in all_points.keys():
            for point in all_points[name]:
                if point["pareto"]:
                    plt.scatter(point["x"], point["y"], label=name)
                    plt.text(point["x"], point["y"], name)
        plt.xlabel(self.metrics[0])
        plt.ylabel(self.metrics[1])
        plt.title("Pareto frontier")
        plt.show()

    def _plot_pareto_3d(self):
        """
        Plot the Pareto frontier.

        Returns:
        - plotly.graph_objects.Figure: The Plotly figure.
        """
        all_points = self.get_pareto_dict()
        ax = plt.axes(projection="3d")
        for name in all_points.keys():
            for point in all_points[name]:
                if point["pareto"]:
                    ax.scatter(point["x"], point["y"], point["z"], label=name)
                    ax.text(point["x"], point["y"], point["z"], name)

        ax.set_xlabel(self.metrics[0])
        ax.set_ylabel(self.metrics[1])
        ax.set_zlabel(self.metrics[2])
        ax.set_title("Pareto frontier")
        plt.show()

    def plot_all_trials(self):
        """
        Plot all trials.

        Returns:
        - plotly.graph_objects.Figure: The Plotly figure.
        """
        all_points = self._retrieve_all_points()

        plt.figure(figsize=(10, 10))

        if len(self.metrics) == 2:

            for name in all_points.keys():

                points = all_points[name]

                x = [point["x"] for point in points]
                y = [point["y"] for point in points]

                plt.scatter(x, y, label=name)

            plt.xlabel(self.metrics[0])
            plt.ylabel(self.metrics[1])
            plt.title("All trials")
            plt.show()

        elif len(self.metrics) == 3:

            ax = plt.axes(projection="3d")

            for name in all_points.keys():

                points = all_points[name]

                x = [point["x"] for point in points]
                y = [point["y"] for point in points]
                z = [point["z"] for point in points]

                ax.scatter(x, y, z, label=name)

            ax.set_xlabel(self.metrics[0])
            ax.set_ylabel(self.metrics[1])
            ax.set_zlabel(self.metrics[2])
            ax.set_title("All trials")
            plt.show()

        else:
            raise ValueError("All trials plot only implemented for 2 or 3 metrics")

    def get_pareto_points_params(self):
        """
        Get the Pareto points and their parameters.

        Returns:
        - dict: Dictionary containing
        """

        self.pareto_params = {}

        if not hasattr(self, "pareto_dict"):
            self.get_pareto_dict()

        for name in self.search_spaces.keys():
            df = exp_to_df(self.search_spaces[name]["experiment"])
            for i, point in enumerate(self.pareto_dict[name]):
                if point["pareto"]:
                    self.pareto_params[name + "_" + str(i)] = df.iloc[i]

        return self.pareto_params
