from torch.utils.data import DataLoader
from torchvision import datasets, transforms
import matplotlib.pyplot as plt
import numpy as np
from torch.utils.data import TensorDataset
import torch
from typing import Tuple

def get_cifar10(
    path: str,
    batch_size: int = 32,
    shuffle: bool = True,
    transform: transforms = transforms.Compose(
        [transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
    ),
) -> tuple[DataLoader, DataLoader]:
    """
    Retrieves the CIFAR-10 dataset and returns the train and test data loaders.

    Args:
        path (str): The root directory where the dataset will be stored.
        batch_size (int, optional): The batch size for the data loaders. Defaults to 32.
        shuffle (bool, optional): Whether to shuffle the data. Defaults to True.
        transform (torchvision.transforms, optional): The data transformations to apply. Defaults to a composition of ToTensor and Normalize.

    Returns:
        tuple[DataLoader, DataLoader]: A tuple containing the train and test data loaders.
    """

    trainset = datasets.CIFAR10(
        root=path, train=True, download=True, transform=transform
    )
    trainloader = DataLoader(
        trainset, batch_size=batch_size, shuffle=shuffle, num_workers=2
    )
    testset = datasets.CIFAR10(
        root=path, train=False, download=True, transform=transform
    )
    testloader = DataLoader(
        testset, batch_size=batch_size, shuffle=shuffle, num_workers=2
    )
    return trainloader, testloader


def plot_cifar_images(classes: list[str], trainloader: DataLoader) -> None:
    """
    Plot CIFAR images for each class.

    Args:
        classes (list[str]): List of class names.
        trainloader (DataLoader): DataLoader containing the CIFAR dataset.

    Returns:
        None
    """
    plt.figure(figsize=(10, 10))
    for i in range(len(classes)):
        plt.subplot(len(classes) // 5 + 1, 5, i + 1)
        plt.xticks([])
        plt.yticks([])
        plt.grid(False)
        plt.imshow(trainloader.dataset.data[trainloader.dataset.targets.index(i)])
        plt.xlabel(classes[i])
    plt.show()

def make_train_test_loader(
    X_train: np.ndarray,
    y_train: np.ndarray,
    X_test: np.ndarray,
    y_test: np.ndarray,
    batch_size: int = 64,
    validation_size: float = 0,
    random_seed: int = 42,
) -> tuple[DataLoader, DataLoader]:
    """
    Creates train and test data loaders from the given data.

    Args:
        X_train (np.ndarray): The training data.
        y_train (np.ndarray): The training labels.
        X_test (np.ndarray): The test data.
        y_test (np.ndarray): The test labels.
        batch_size (int, optional): The batch size for the data loaders. Defaults to 64.

    Returns:
        tuple[DataLoader, DataLoader]: A tuple containing the train and test data loaders.
    """
    X_train = torch.tensor(X_train, dtype=torch.float32)
    y_train = torch.tensor(y_train, dtype=torch.float32)
    X_test = torch.tensor(X_test, dtype=torch.float32)
    y_test = torch.tensor(y_test, dtype=torch.float32)

    if validation_size == 0:

        train_dataset = TensorDataset(X_train, y_train)
        test_dataset = TensorDataset(X_test, y_test)

        train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
        test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False)

        return train_loader, test_loader
    else:
        assert (
            validation_size > 0 and validation_size < 1
        ), "Validation size must be between 0 and 1"

        np.random.seed(random_seed)

        n_train = len(X_train)
        indices = list(range(n_train))
        split = int(np.floor(validation_size * n_train))
        np.random.shuffle(indices)
        train_idx, valid_idx = indices[split:], indices[:split]

        train_dataset = TensorDataset(X_train[train_idx], y_train[train_idx])
        valid_dataset = TensorDataset(X_train[valid_idx], y_train[valid_idx])

        train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
        valid_loader = DataLoader(valid_dataset, batch_size=batch_size, shuffle=False)

        test_dataset = TensorDataset(X_test, y_test)
        test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False)

        return train_loader, valid_loader, test_loader


def make_validation_loader(
    train_loader: DataLoader, validation_size: float = 0.15, random_seed: int = 42
) -> tuple[DataLoader, DataLoader]:
    """
    Creates train and validation data loaders from the given train loader.

    Args:
        train_loader (DataLoader): The training data loader.
        validation_size (float, optional): The size of the validation set as a fraction of the training set. Defaults to 0.15.

    Returns:
        tuple[DataLoader, DataLoader]: A tuple containing the train and validation data loaders.
    """

    split = int(np.floor((1 - validation_size) * len(train_loader)))

    for i, (batch_data, batch_target) in enumerate(train_loader):
        if i == 0:
            X_train = batch_data
            y_train = batch_target
        elif i < split:
            X_train = torch.cat((X_train, batch_data), 0)
            y_train = torch.cat((y_train, batch_target), 0)
        elif i == split:
            X_val = batch_data
            y_val = batch_target
        else:
            X_val = torch.cat((X_val, batch_data), 0)
            y_val = torch.cat((y_val, batch_target), 0)

    train_dataset = TensorDataset(X_train, y_train)
    valid_dataset = TensorDataset(X_val, y_val)

    train_loader = DataLoader(
        train_dataset, batch_size=train_loader.batch_size, shuffle=True
    )
    valid_loader = DataLoader(
        valid_dataset, batch_size=train_loader.batch_size, shuffle=False
    )

    return train_loader, valid_loader

def calculate_convlayer_out_size(
    height: int,
    width: int,
    kernel_size: int,
    padding: int,
    stride: int,
    dilation: int,
    pooling_kernel_size: int = 1,
    pooling_stride: int = 1,
) -> Tuple[int, int]:
    """
    Calculate the output size of a convolutional layer.

    Args:
        height (int): The height of the input tensor.
        width (int): The width of the input tensor.
        kernel_size (int): The size of the kernel.
        padding (int): The padding size.
        stride (int): The stride size.
        dilation (int): The dilation size.
        pooling_kernel_size (int, optional): The size of the pooling kernel. Defaults to 1.

    Returns:
        Tuple[int, int]: The height and width of the output tensor.
    """
    out_height = (
        (height + 2 * padding - dilation * (kernel_size - 1) - 1) // stride
    ) + 1
    out_width = ((width + 2 * padding - dilation * (kernel_size - 1) - 1) // stride) + 1

    out_height = ((out_height - pooling_kernel_size) // pooling_stride) + 1
    out_width = ((out_width - pooling_kernel_size) // pooling_stride) + 1

    return out_height, out_width
