from typing import List, Dict, Optional, Tuple
import pandas as pd
import numpy as np
from codecarbon import EmissionsTracker
from imblearn.over_sampling import SMOTE
import sklearn
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV, train_test_split
from sklearn.metrics import accuracy_score, f1_score
import warnings

warnings.filterwarnings("ignore")


class PipelineProfiler:
    def __init__(
        self,
        data: pd.DataFrame,
        target: str,
        sampling_strategy: str,
        k_neighbors: int = 3,
        feature_selection: bool = False,
        selector: Optional[int] = None,
        model: Optional[sklearn.base.BaseEstimator] = None,
        preprocessor: Optional[sklearn.pipeline.Pipeline] = None,
        search_method: str = "none",
        param_grid: Optional[Dict] = None,
        imputer: Optional[sklearn.impute._base] = None,
        cols_to_impute: Optional[List[str]] = None,
    ):
        """
        Initializes the PipelineProfiler object.

        Args:
            data (pd.DataFrame): The input data.
            target (str): The target column name.
            model (Optional[sklearn.base.BaseEstimator], optional): The model to use. Defaults to None.
            preprocessor (Optional[sklearn.pipeline.Pipeline], optional): The preprocessor to use. Defaults to None.
            search_method (str, optional): The search method to use. Defaults to 'grid'.
            param_grid (Optional[Dict], optional): The parameter grid to use. Defaults to None.
            imputer (Optional[Union[sklearn.impute.SimpleImputer, sklearn.impute.KNNImputer]], optional): The imputer to use. Defaults to None.
            cols_to_impute (Optional[List[str]], optional): The columns to impute. Defaults to None.
        """
        self.original_data = data
        self.target = target
        self.sampling_strategy = sampling_strategy
        self.k_neighbors = k_neighbors
        self.model = model
        self.preprocessor = preprocessor
        self.imputer = imputer
        self.cols_to_impute = cols_to_impute
        self._validate_imputer_and_cols_to_impute()
        self.search_method = search_method
        self.param_grid = param_grid
        self._validate_search_method_and_param_grid()
        self.results = {}
        self.imputed = False
        self.apply_feature_selection = feature_selection
        self.selector = selector
        self.fitted = False
        self.preprocessed = False
        self.X_train, self.X_test, self.y_train, self.y_test = None, None, None, None
        self.y_pred = None
        (
            self.train_impute_emissions,
            self.train_preprocessing_emissions,
            self.train_emissions,
        ) = (0, 0, 0)
        (
            self.inference_impute_emissions,
            self.inference_preprocessing_emissions,
            self.inference_emissions,
        ) = (0, 0, 0)
        self.train_total_emissions, self.inference_total_emissions = 0, 0
        self.splitted = False

    def _validate_imputer_and_cols_to_impute(self):
        if self.imputer is not None and self.cols_to_impute is None:
            raise ValueError(
                "Please provide the columns to impute or remove the imputer."
            )

        if self.cols_to_impute is not None and self.imputer is None:
            print("WARNING: Columns to impute provided but no imputer set.")

        missing_columns = [
            col for col in self.cols_to_impute if col not in self.original_data
        ]
        if self.cols_to_impute is not None and missing_columns:
            raise ValueError(f"Columns {', '.join(missing_columns)} not found in data.")

    def _validate_search_method_and_param_grid(self):
        if self.search_method not in ["grid", "random", "none"]:
            raise ValueError(
                'Invalid search method. Please use "grid", "random", or "none".'
            )
        if self.search_method != "none" and self.param_grid is None:
            raise ValueError(
                "Please provide a parameter grid for grid or random search."
            )
        if self.search_method == "none" and self.param_grid is not None:
            print("WARNING: Parameter grid provided but search method is set to none.")

        # check if the parameters in the grid are valid
        if self.param_grid is not None:
            for key in self.param_grid:
                if key not in self.model.get_params().keys():
                    raise ValueError(f"Parameter {key} not found in model parameters.")

    def impute_data(self) -> None:
        """
        Imputes missing values in the specified columns
        """
        if not self.splitted:
            self.split_data()

        if self.imputed:
            print("Data already imputed.")
            return
        print("Imputing data...")

        if self.imputer is None:
            self.train_impute_emissions = 0
        else:
            tracker = EmissionsTracker(log_level="critical", save_to_file=False)
            tracker.start()
            self.X_train[self.cols_to_impute] = self.imputer.fit_transform(
                self.X_train[self.cols_to_impute]
            )
            self.train_impute_emissions = tracker.stop() * 1000

        self.imputed = True

    def split_data(self, test_size: float = 0.2) -> None:
        """
        Splits the data into training and testing sets.

        Args:
            test_size (float): The size of the test set.
        """
        if self.splitted:
            print("Data already splitted.")
            return
        print("Splitting data...")
        self.data = self.original_data.copy()
        X = self.data.drop(self.target, axis=1)
        y = self.data[self.target]

        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(
            X, y, test_size=test_size, random_state=42
        )

        self.splitted = True

    def oversample_data(self) -> None:
        match self.sampling_strategy:
            case "equal":
                oversample = SMOTE(
                    k_neighbors=self.k_neighbors, sampling_strategy="not majority"
                )
                self.X_train, self.y_train = oversample.fit_resample(
                    self.X_train, self.y_train
                )
            case "custom":
                custom_strategy = {
                    "MOY-INFER": self.y_train.value_counts()["MOY-INFER"] + 1,
                    "MOY-SUPER": round(self.y_train.value_counts()["MOY-SUPER"] * 1.2),
                    "LUXE": round(self.y_train.value_counts()["LUXE"] * 1.2),
                    "SUPERIEURE": round(
                        self.y_train.value_counts()["SUPERIEURE"] * 1.2
                    ),
                    "INFERIEURE": round(
                        self.y_train.value_counts()["INFERIEURE"] * 1.2
                    ),
                    "ECONOMIQUE": round(
                        self.y_train.value_counts()["ECONOMIQUE"] * 1.2
                    ),
                    "MOY-INFERIEURE": round(
                        self.y_train.value_counts()["MOY-INFERIEURE"] * 1.2
                    ),
                }
                oversample = SMOTE(
                    k_neighbors=self.k_neighbors, sampling_strategy=custom_strategy
                )
                self.X_train, self.y_train = oversample.fit_resample(
                    self.X_train, self.y_train
                )
            case _:
                print("No oversampling performed")

    def feature_selection(self) -> None: 
        _ = self.selector.fit_transform(self.X_train, self.y_train)


    def preprocess_data(self) -> None:
        """
        Preprocesses the data using the specified preprocessor.
        """
        self.split_data()
        self.impute_data()

        if self.preprocessed:
            print("Data already preprocessed.")
            return
        print("Preprocessing data...")

        if self.preprocessor is not None:
            tracker = EmissionsTracker(log_level="critical", save_to_file=False)
            tracker.start()
            self.X_train = self.preprocessor.fit_transform(self.X_train)
            self.train_preprocessing_emissions = tracker.stop() * 1000
        else:
            self.train_preprocessing_emissions = 0
        self.preprocessed = True

        self.oversample_data()
        print("Data preprocessed.")


    def fit(self) -> Dict:
        """
        Fits the model to the training data.
        """
        if self.fitted:
            print("Model already fitted.")
        else:
            if self.search_method == "grid":
                search = GridSearchCV(self.model, self.param_grid, cv=5, n_jobs=-1)
            elif self.search_method == "random":
                search = RandomizedSearchCV(
                    self.model, self.param_grid, cv=5, n_jobs=-1
                )
            elif self.search_method == "none" or self.search_method is None:
                search = self.model
            else:
                print("No proper search method defined")
                return {}

            self.preprocess_data()

            tracker = EmissionsTracker(log_level="critical", save_to_file=False)
            print("Fitting model...")
            tracker.start()
            if self.apply_feature_selection:
                self.feature_selection()
                self.X_train = self.selector.transform(self.X_train)
            search.fit(self.X_train, self.y_train)
            if self.search_method != "none":
                self.model = search.best_estimator_
            self.train_emissions = tracker.stop() * 1000
            self.fitted = True
            self.train_total_emissions = (
                self.train_impute_emissions
                + self.train_preprocessing_emissions
                + self.train_emissions
            )

            self.train_dict = {
                "impute_emissions": self.train_impute_emissions,
                "preprocessing_emissions": self.train_preprocessing_emissions,
                "fitting_emissions": self.train_emissions,
            }
            print("Model fitted.")
            return {
                "model": search,
                "impute_emissions": self.train_impute_emissions,
                "preprocessing_emissions": self.train_preprocessing_emissions,
                "fitting_emissions": self.train_emissions,
            }

    def predict(
        self, X: Optional[pd.DataFrame] = None
    ) -> tuple[np.ndarray, Dict[str, float]]:
        """
        Predicts the target values for the input data.

        Args:
            X (pd.DataFrame, optional): The input data. Defaults to None.

        Returns:
            np.ndarray: The predicted target values.
        """
        print("Predicting...")

        if not self.fitted:
            print("WARNING: Model not fitted")
        if not self.preprocessed:
            print("WARNING: Data not preprocessed")
        if not self.imputed:
            print("WARNING: Data not imputed")

        if X is None:
            print("WARNING: No input data provided. Using test set.")
            X = self.X_test.copy()
            print('here is X shape : ', X.shape)

        if self.imputer is not None:
            tracker = EmissionsTracker(log_level="critical", save_to_file=False)
            tracker.start()
            X[self.cols_to_impute] = self.imputer.transform(X[self.cols_to_impute])
            self.inference_impute_emissions = tracker.stop() * 1000
        else:
            self.inference_impute_emissions = 0

        if self.preprocessor is not None:
            tracker = EmissionsTracker(log_level="critical", save_to_file=False)
            tracker.start()
            X = self.preprocessor.transform(X)
            self.inference_preprocessing_emissions = tracker.stop() * 1000
        else:
            self.inference_preprocessing_emissions = 0

        tracker = EmissionsTracker(log_level="critical", save_to_file=False)
        tracker.start()
        if self.apply_feature_selection:
            X = self.selector.transform(X)
        y_pred = self.model.predict(X)
        self.inference_emissions = tracker.stop() * 1000
        self.inference_total_emissions = (
            self.inference_impute_emissions
            + self.inference_preprocessing_emissions
            + self.inference_emissions
        )

        self.predict_dict = {
            "impute_emissions": self.inference_impute_emissions,
            "preprocessing_emissions": self.inference_preprocessing_emissions,
            "fitting_emissions": self.inference_emissions,
        }

        self.y_pred = y_pred

        print("Prediction done.")

        return self.y_pred, self.predict_dict

    def reset(
        self, impute: bool = False, preprocessing: bool = False, fit: bool = False
    ) -> None:
        """
        Resets the PipelineProfiler object.

        Args:
            impute (bool, optional): Whether to reset the imputation. Defaults to True.
            preprocessing (bool, optional): Whether to reset the preprocessing. Defaults to True.
            fit (bool, optional): Whether to reset the fitting. Defaults to True.
        """
        self.y_pred = None

        if impute:
            self.imputed = False
            self.preprocessed = False
            self.fitted = False
            self.splitted = False
            return
        if preprocessing:
            self.preprocessed = False
            self.fitted = False
            self.splitted = False
            return
        if fit:
            self.fitted = False
            return

    def set_model(self, model: sklearn.base.BaseEstimator) -> None:
        """
        Sets the model to use.

        Args:
            model (sklearn.base.BaseEstimator): The model to use.
        """
        self.model = model
        self.reset(fit=True)

    def set_preprocessor(self, preprocessor: sklearn.compose.ColumnTransformer) -> None:
        """
        Sets the preprocessor to use.

        Args:
            preprocessor (sklearn.pipeline.Pipeline): The preprocessor to use.
        """
        self.preprocessor = preprocessor
        self.reset(preprocessing=True)

    def set_imputer(
        self, imputer: sklearn.impute._base, cols_to_impute: Optional[List[str]] = None
    ) -> None:
        """
        Sets the imputer to use.

        Args:
            imputer (Union[sklearn.impute.SimpleImputer, sklearn.impute.KNNImputer]): The imputer to use.
            cols_to_impute: Columns to impute
        """
        if cols_to_impute is not None:
            self.cols_to_impute = cols_to_impute
        self.imputer = imputer
        self._validate_imputer_and_cols_to_impute()
        self.reset(impute=True)

    def set_param_grid(self, param_grid: Dict) -> None:
        """
        Sets the parameter grid to use.

        Args:
            param_grid (Dict): The parameter grid to use.
        """
        self.param_grid = param_grid
        self._validate_search_method_and_param_grid()
        self.reset(fit=True)

    def set_search_method(
        self, search_method: str, param_grid: Optional[Dict] = None
    ) -> None:
        """
        Sets the search method to use.

        Args:
            :param search_method: The search method to use.
            :param param_grid: the hyperparameters to explore in the tuning method
        """
        if param_grid is not None:
            self.param_grid = param_grid
        if search_method not in ["grid", "random", "none"]:
            raise ValueError(
                'Invalid search method. Please use "grid", "random", or "none".'
            )
        self.search_method = search_method
        self._validate_search_method_and_param_grid()
        self.reset(fit=True)

    def score(
        self,
        y_pred: Optional[np.ndarray] = None,
        y_true: Optional[np.ndarray] = None,
        metric: str = "f1",
    ) -> float:
        """
        Scores the model.

        Args:
            y_pred (np.ndarray, optional): The predicted target values.
            y_true (np.ndarray, optional): The true target values. Defaults to None.
            metric: Metric to evaluate

        Returns:
            float: The score.
        """
        if y_true is None:
            print("WARNING: True values not provided. Using test set true values.")
            y_true = self.y_test.copy()
        if y_pred is None:
            try:
                y_pred = self.y_pred.copy()
            except AttributeError:
                print(
                    "WARNING: Predicted values not provided. Using test set predicted values."
                )
                y_pred = self.predict()[0]

        if metric == "accuracy":
            y_pred_acc = np.argmax(y_pred, axis=1)
            score = accuracy_score(y_true, y_pred_acc)
            self.accuracy = score
            return score
        if metric == "f1":
            score = f1_score(y_true, y_pred, average="weighted")
            self.f1_score = score
            return score
        # Add more metrics here

    def get_results(self, option="all") -> Dict:
        """
        Returns the results of the PipelineProfiler object.

        Returns:
            Dict: The results.
        """
        match option:
            case "train":
                return self.train_dict
            case "predict":
                return self.predict_dict
            case _:
                return {"train": self.train_dict, "predict": self.predict_dict}
