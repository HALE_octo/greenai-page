

class DatasetColumns:
    hybride = "hybride"
    ptcl = "ptcl"
    typ_boite = "typ_boite"
    puiss_admin_98 = "puiss_admin_98"
    puiss_max = "puiss_max"
    nb_rapp = "nb_rapp"
    conso_mixte = "conso_mixte"
    co2 = "co2"
    co_typ_1 = "co_typ_1"
    hcnox = "hcnox"
    masse_ordma_min = "masse_ordma_min"

    categorical_features = [hybride, ptcl, typ_boite]

    numerical_features = [
        puiss_admin_98, puiss_max, nb_rapp, conso_mixte, co2, co_typ_1, hcnox, ptcl, masse_ordma_min,
    ]

    columns_to_impute = [conso_mixte, co2, co_typ_1, hcnox, ptcl]
