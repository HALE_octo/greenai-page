import os
from typing import Dict, Optional

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from IPython.display import display, Markdown

from lib_dojo.dataset_columns import DatasetColumns


COLUMNS_TO_DROP = [
    "date_maj", "cnit", "lib_mod_doss", "dscom", "tvv", "masse_ordma_max", "champ_v9", "typ_boite_nb_rapp",
    "Carrosserie", "lib_mrq", "lib_mod", "cod_cbr", "nox", "hc", "conso_urb", "conso_exurb"
]


def _clean_carlab_dataset(df_carlab: pd.DataFrame) -> pd.DataFrame:
    df_carlab[DatasetColumns.typ_boite] = df_carlab["typ_boite_nb_rapp"].str[0]
    df_carlab[DatasetColumns.nb_rapp] = df_carlab["typ_boite_nb_rapp"].str[2].replace(".", 0).astype("int")
    df_carlab[DatasetColumns.puiss_max] = df_carlab[DatasetColumns.puiss_max].str.replace(",", ".").astype("float")

    return df_carlab.drop(COLUMNS_TO_DROP, axis=1, errors="ignore")


def load_cleaned_carlab_data(data_path: str, load_and_display_lexique: bool = True):

    if load_and_display_lexique:
        lexique = pd.read_excel(os.path.join(data_path, "carlab-annuaire-variable.xlsx"))
        lexique = lexique[~lexique["nom-colonne"].isin(COLUMNS_TO_DROP)].reset_index(drop=True)
        display(Markdown("**Description officielle des colonnes**"))
        display(lexique)

    df = pd.read_csv(os.path.join(data_path, "mars-2014-complete.csv"), sep=";", encoding="latin-1")
    df = _clean_carlab_dataset(df)

    display(Markdown("**DataFrame head:**"))
    display(df.head())
    display(Markdown(f"**DataFrame shape: {df.shape}**"))

    return df


def scatterplot_score_emissions(comparison_dict: Dict[str, dict], log_scale: bool = False):
    """
    Scatterplot of the score vs CO2 emissions

    Parameters
    ----------
    comparison_dict : Dict
        Dictionary containing the training emissions, inference emissions and score
    log_scale : Bool
        If True, plots in logarithmic scale

    Returns
    -------
    None
    """

    inference_emissions = [model_stats["inference"] for _, model_stats in comparison_dict.items()]
    training_emissions = [model_stats["train"] for _, model_stats in comparison_dict.items()]
    scores = [model_stats["score"] for _, model_stats in comparison_dict.items()]

    fig, axs = plt.subplots(1, 2, sharey='row')
    ax1, ax2 = axs
    plt.sca(ax1)
    plt.title('Training')
    plt.sca(ax2)
    plt.title('Inference')

    plt.suptitle("Emissions vs Score")

    ax1.scatter(
        scores, training_emissions, cmap="RdYlGn_r", c=training_emissions
    )
    ax1.set_ylabel("Training CO2 emissions (g)")
    for i, txt in enumerate([d for d in comparison_dict]):
        ax1.annotate(txt, (scores[i], training_emissions[i]))

    ax2.scatter(
        scores, inference_emissions, cmap="RdYlGn_r", c=inference_emissions
    )
    ax2.set_ylabel("Inference CO2 emissions (g)")
    for i, txt in enumerate([d for d in comparison_dict]):
        ax2.annotate(txt, (scores[i], inference_emissions[i]))

    if log_scale:
        plt.yscale('log')

    plt.show()

class DataViz:
    """
    This class provides some vizualisations functions
    to better help you understand the dataset
    """

    def __init__(self):
        pass

    def plot_scatter(
        self,
        data: pd.DataFrame,
        col_x: str = "",
        col_y: str = "co2",
        hue: Optional[str] = None,
    ) -> None:
        """
        Plots a scatter plot to visualize the relationship between two variables.

        Args:
            data (pd.DataFrame): The input data.
            col_x (str): The column name for the x-axis. Default is ''.
            col_y (str): The column name for the y-axis. Default is 'c02'.
            hue (str, optional): The column name to color points by. Default is None.

        Returns:
            None
        """
        # Create a figure for the plot with a specific size
        plt.figure(figsize=(10, 6))
        # Create a scatter plot using seaborn
        sns.scatterplot(data=data, x=col_x, y=col_y, hue=hue, palette="Set1")
        # Set labels for x and y axes
        plt.xlabel(col_x)
        plt.ylabel(col_y)
        # Ensure tight layout
        plt.tight_layout()
        plt.show()

    def plot_bar(self, data: pd.DataFrame) -> None:
        """
        Plots a bar chart showing the number of unique values in categorical columns.

        Args:
            data (pd.DataFrame): The input data.

        Returns:
            None
        """
        ax = sns.barplot(x="Column", y="Modalité", data=data)
        plt.title("Number of unique values in categorical columns")
        plt.xlabel("Columns")
        plt.ylabel("Unique Count")
        plt.xticks(rotation=45, ha="right")
        for index, row in data.iterrows():
            ax.text(index, row["Modalité"], row["Modalité"], ha="center")
        plt.show()

    def countplot_categorical_ftr(self, data: pd.DataFrame) -> pd.DataFrame:
        """
        Counts the number of unique values in categorical columns and plots a bar chart.

        Args:
            data (pd.DataFrame): The input data.

        Returns:
            pd.DataFrame: A DataFrame containing the column names and their corresponding unique counts.
        """
        counts = []
        for column in data.columns:
            if not pd.api.types.is_numeric_dtype(data[column]):
                unique_count = data[column].nunique()
                counts.append((column, unique_count))
        counts_df = (
            pd.DataFrame(counts, columns=["Column", "Modalité"])
            .sort_values(by="Modalité", ascending=False)
            .reset_index()
        )
        self.plot_bar(counts_df)
        return counts_df

    def plot_missing_values(self, data: pd.DataFrame) -> None:
        """
        Retrieves the number of missing values for each feature and then plot a bar chart.

        Args:
            data (pd.DataFrame): The input data.

        Returns:
            Nothing, plot a hist chart.
        """
        missing_values_count = data.isnull().sum()

        plt.figure(figsize=(10,6))
        sns.barplot(x=missing_values_count.index, y=missing_values_count.values, color='skyblue')

        plt.title('Bar Plot of Missing Values Count')
        plt.xlabel('Columns')
        plt.ylabel('Number of Missing Values (log scale)')
        plt.xticks(rotation=90)
        plt.yscale('log')
        plt.show()

    def plot_numerical_distribution(self, data: pd.DataFrame, column: str) -> None:
        """
        Plot distribution of a numerical column

        Args:
            data (pd.Dataframe): The input data.
            column: the column to plot
        """
        plt.figure(figsize=(6,4))

        sns.histplot(data[column], kde=True, color='skyblue', edgecolor='black')
        plt.title(f'Distribution of {column}')
        plt.ylabel('Frequency')

        plt.legend()
        plt.show()


    def plot_categorical_distribution(self, data: pd.DataFrame, column: str, target_column: str) -> None:
        """
        Plot distribution of a categorical column

        Args:
            data (pd.Dataframe): The input data.
            column: the column to plot
        """
        plt.figure(figsize=(10, 6))
        sns.countplot(data=data, x=column, hue=target_column, palette='Set2')

        plt.title(f'Distribution of {column} with {target_column} as hue')
        plt.xlabel(column)
        plt.ylabel('Count')
        plt.legend(title=target_column)
        plt.xticks(rotation=90)
        plt.yscale('log')
        plt.show()
