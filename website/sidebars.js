module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Green AI - Methodology',
      items: [
        'methodology/codecarbon',
        'methodology/mlflow',
      ],
    },
    {
      type: 'category',
      label: 'Green AI - Easy to Implement',
      items: [
        'easy/tips-and-tricks',
        'easy/quantization',
        'easy/layer-fusion',
        'easy/kd',
      ],
    },
    {
      type: 'category',
      label: 'Green AI - Advanced',
      items: [
        'advanced/pruning',
        'advanced/nas',
  ],
},
{
  type: 'category',
  label: 'Contributing',
  items: [
    'contributing/getting-started',
    'contributing/create-a-page',
    'contributing/create-a-document',
    'contributing/create-a-blog-post',
    'contributing/markdown-features',
    'contributing/thank-you',
  ],
},
  ]
};
