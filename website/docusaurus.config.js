/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Green AI',
  tagline: 'Quick Start to simple methods to reduce the carbon footprint of your AI models',
  url: 'https://HALE_octo.gitlab.io',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'Octo',
  projectName: 'Green AI',
  themeConfig: {
    navbar: {
      title: 'Green AI for Octo',
      logo: {
        alt: 'My Site Logo',
        src: 'img/logo-octo.jpeg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {to: 'blog', label: 'Blog', position: 'left'},
        {
          href: 'https://gitlab.octo.tools/tous-les-octos/archives-migration/les-bg-de-la-data/s-s-all/projets-interne/greenai',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Getting Started',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Utils',
          items: [
            {
              label: 'Green AI Formation Made by Octo',
              href: 'https://script.google.com/a/octo.com/macros/s/AKfycbwcl4OYe4fiN0oFXtCCj0CVH_rhrmCYqigDFFTCeRBcpvcGNkYjdLCO_FhEwu73zZjr/exec?do=training&id=392',
            },
            {
              label: 'A systematic review of Green AI',
              href: 'https://arxiv.org/pdf/2301.11047',
            },
          ],
        },
        {
          title: 'Contributing',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            {
              label: 'Mattermost',
              href: 'https://mattermost.octo.tools/data-ai/channels/green-ai',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Octo's Green AI Team. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.octo.tools/tous-les-octos/archives-migration/les-bg-de-la-data/s-s-all/projets-interne/greenai/-/tree/page/website/docs?ref_type=heads',
        },
        blog: {
          showReadingTime: true,
          editUrl:
            'https://gitlab.octo.tools/tous-les-octos/archives-migration/les-bg-de-la-data/s-s-all/projets-interne/greenai/-/tree/page/website/blog?ref_type=heads',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  customFields: {
    webpack: {
      configure: (webpackConfig) => {
        webpackConfig.module.rules.push({
          test: /\.mjs$/,
          include: /node_modules/,
          type: "javascript/auto",
        });
        return webpackConfig;
      },
    },
  },
};
