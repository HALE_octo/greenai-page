---
title: Knowledge Distillation
slug: kd
---

## Part 1: Understanding Knowledge Distillation

Knowledge distillation is a technique in machine learning where a smaller, simpler model (the student) is trained to replicate the behavior of a larger, more complex model (the teacher). This approach leverages the insights learned by the teacher model to create a more efficient student model.

### Why Use Knowledge Distillation?

Knowledge distillation is important for several reasons:
- **Model Compression:** It helps in reducing the size of the model, making it more suitable for deployment on resource-constrained devices.
- **Inference Speed:** Distilled models generally have faster inference times.
- **Transfer Learning:** It allows the student model to learn from the pre-trained teacher model, leveraging its knowledge.

### How does it works ?

The distillation process involves transferring knowledge from a complex teacher model to a simpler student model by using a loss function that captures the similarities between the teacher's predictions and the student's predictions.

- Teacher Model: The process begins with a well-trained teacher model, often a large and complex neural network that achieves high performance on a given task. The teacher model serves as the source of knowledge to be transferred to the student.

- Student Model: The student model is typically a smaller and simpler neural network architecture compared to the teacher model. Its objective is to learn from the teacher's knowledge and generalize it to the target task.

- Loss Function: The distillation process relies on a specialized loss function that not only penalizes the discrepancies between the student's predictions and the ground truth labels but also incorporates a term that encourages the student to mimic the soft probabilities output by the teacher model. This softening of the target labels allows the student to learn from the rich, distributed knowledge encoded in the teacher's predictions, beyond simple class labels.

- Training: During training, the student model is trained on the dataset using the modified loss function. By minimizing this distillation loss in addition to the standard supervised loss, the student gradually acquires the knowledge distilled from the teacher, resulting in a model that generalizes well and exhibits performance comparable to or even surpassing that of the teacher model, despite its smaller size.

![Knowledge Distillation's Training Process](img/kd_train.png)


## Part 2: Pros and Cons of Knowledge Distillation

### Pros

- **Efficiency:** Smaller models require less memory and computational resources.
- **Speed:** Faster inference makes it suitable for real-time applications.
- **Practicality:** Easier to deploy on edge devices and in production environments.
- **Performance:** Can maintain high levels of accuracy despite being smaller.

### Cons

- **Complexity:** The distillation process can be complex and requires careful tuning.
- **Training Time:** It may take longer to train the student model to achieve desired performance.
- **Dependency:** The quality of the student model heavily depends on the performance of the teacher model.
- **Limited Improvement:** In some cases, the distilled model may not significantly outperform a directly trained small model.


## Part 3: Implementing Knowledge Distillation in PyTorch

### Setting up the Environment

To start using knowledge distillation in PyTorch, you need to have PyTorch installed in your environment.

```shell
pip install torch
```

### Updating the loss calculation

Now, we will create a custom loss function designed to transfer the knowledge between the student and the teacher model.

```python
def distillation_loss(student_outputs, teacher_outputs, true_labels, criterion,
    T: float = 2.0,
    alpha: float = 0.5,
):
    """
    Calculates the distillation loss for knowledge distillation.

    Args:
        student_outputs (torch.Tensor): The output logits of the student model.
        teacher_outputs (torch.Tensor): The output logits of the teacher model.
        true_labels (torch.Tensor): The true labels for the input data.
        criterion: The loss criterion used for the student model.
        T (float, optional): The temperature parameter for distillation. Defaults to 2.0, as recommended by PyTorch doc.
        alpha (float, optional): The weight given to the teacher loss. Defaults to 0.5.

    Returns:
        torch.Tensor: The calculated distillation loss.

    """

    # Calculate the soft targets using softmax with temperature T
    # (the T is used to have more distributed probabilities)
    soft_targets = torch.nn.functional.softmax(teacher_outputs / T, dim=1)

    # Calculate the soft probabilities using log softmax with temperature T
    # we use the log of the softmax for better mathematical stability
    soft_prob = torch.nn.functional.log_softmax(student_outputs / T, dim=1)

    # Calculate the teacher loss, it is the sum of the differences between both models
    # over the probabilities ponderated by their importance
    teacher_loss = (
        torch.sum(soft_targets * (soft_targets.log() - soft_prob))
        / soft_prob.size()[0]
        * T**2
    )

    # Calculate the student loss using the provided criterion
    student_loss = criterion(student_outputs, true_labels)

    # Calculate the overall loss as a weighted sum of teacher loss and student loss
    loss = alpha * teacher_loss + (1 - alpha) * student_loss

    return loss
```

Now, we only have to put this new loss in our base training function. Note that we also need to calculate the teacher outputs now, make sure to use the 'with torch.no_grad():' attribute while doing so.

## Part 4: Best Practices and Considerations

### When to Use Knowledge Distillation

- Resource Constraints : Use knowledge distillation when you need to deploy models on devices with limited resources.
- Speed Requirements : When faster inference is crucial for the application.
- Transfer Learning: To leverage the knowledge of a pre-trained large model.
### Fine-Tuning the Student Model
- Hyperparameters: Experiment with different values of temperature (T) and alpha to find the best balance between the distillation loss and the standard loss.
- Model Architecture: The student model should have enough capacity to approximate the teacher model’s behavior.
### Monitoring and Evaluation
- Performance Metrics: Regularly evaluate the student model’s performance on validation datasets.
Model Size and Speed: Compare the size and inference speed of the student model with the teacher model to ensure the benefits of distillation.
- By following these guidelines and implementing knowledge distillation effectively, you can create efficient and deployable models without significantly compromising on performance.