---
title: Metrics
slug: /
---

## Part 1: We improve what we measure

The first step in improving our AI Model is to measure its carbon footprint. By quantifying the energy consumption and CO2 emissions, we can take actionable steps to minimize the environmental impact of our AI models.

### Why Measure?

Measuring the environmental impact of AI models is crucial because:
- AI training can be energy-intensive, leading to high carbon emissions.
- Understanding energy usage helps in optimizing models for better efficiency.
- It aligns with global sustainability goals and corporate social responsibility.

## Part 2 : Introducing CodeCarbon

CodeCarbon is a tool designed to track the energy consumption of your code. By integrating CodeCarbon into your workflow, you can get real-time feedback on the carbon footprint of your AI model training and inference processes.

### Why CodeCarbon ?

We found it was the most advanced library on the subject, with a pretty good methodology :

**Calculation Steps:**

**Estimation of Energy Efficiency (EE):**
- Retrieve from Cloud Provider if possible.
- Otherwise:
  - Detect the region (use default values if detection fails).
  - Use ElectricityMaps API (use default values if API is unavailable).

**Measurement of Electricity Consumption (EC):**
- Detect hardware to obtain electrical power.
- Measure the percentage of utilization.

**Calculation of Carbon Footprint (CF):**
- CF (CO2eq) = EE (CO2eq/kWh) x EC (KWh)

### Setting up CodeCarbon

To start using CodeCarbon, you need to install it in your environment:

```shell
pip install codecarbon
```

### Integrating CodeCarbon into Your Code

Here’s a simple example of how to use CodeCarbon in your Python code:

```python
from codecarbon import EmissionsTracker

tracker = EmissionsTracker()
tracker.start()

# Your AI model training code here

tracker.stop()
```

## Part 3: Other Relevant Measures

In addition to measuring the carbon footprint, it can be relevant to track other metrics such as FLOPs (Floating Point Operations) and the number of parameters in your AI model. These metrics help in understanding the computational complexity and efficiency of your models. Moreover, they are not hardware or cloud providers dependant which is a good point when comparing different models.

### Monitoring FLOPS

You can use the `ptflops` library to measure the MACs (Multiply-Add Operations, usually we have FLOPs = 2 * MACs) of your PyTorch models. 

Here's an example:

```python
import torch
import torch.nn as nn
from ptflops import get_model_complexity_info

model = nn.Sequential(nn.Linear(100, 100))

# Define the input size
input_size = (1, 100)

# Get FLOPS and number of parameters
macs, params = get_model_complexity_info(model, input_size, as_strings=True, print_per_layer_stat=True)

print(f"MACs: {macs}")
print(f"Parameters: {params}")
```

### Monitoring parameters

There are often built-in tools in the most used AI librairies like PyTorch or Keras to get the number of parameters of your AI Models, we invite you checking their documentations :).




